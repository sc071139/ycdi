# label generator

f=open("example_labels.txt","w")

label_num=102

# write for label_map
f.write("label_map:\n")
name_list=["aeroplane"]*8+\
        ["bicycle"]*10+\
        ["boat"]*8+\
        ["bottle"]*7+\
        ["bus"]*12+\
        ["car"]*12+\
        ["chair"]*10+\
        ["sofa"]*10+\
        ["train"]*17+\
        ["tvmonitor"]*8

for i in range(label_num):
    f.write("  {0}: \'{1}\'\n".format(i, name_list[i]+str(i)))

# write for color_map:
f.write("\ncolor_map:\n")
for i in range(label_num):
    f.write("  {0}: [0, 0, 255]\n".format(i))

# write for label_remap
f.write("\nlabel_remap:\n")
for i in range(label_num):
    f.write("  {0}: {1}\n".format(i,i))

f.close()

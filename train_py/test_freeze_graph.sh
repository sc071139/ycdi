# freeze the pretrained model for cpp deployment.
# -p is the path where your previous logs lie
# -l is the new path where you want to put the ffrozen protobuf
# -m is the metrics type (acc/iou) you select for choosing the best model
source test_cfg.sh
./cnn_freeze.py -p cfg/$CFG/logs -l cfg/$CFG/f-logs -m acc

# coding: utf-8

# Copyright 2017 Andres Milioto. All Rights Reserved.
#
#  This file is part of Bonnet.
#
#  Bonnet is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Bonnet is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Bonnet. If not, see <http://www.gnu.org/licenses/>.

""" Abstraction for getting general dataset and putting it in an abstract class
    that can handle any segmentation problem :) You can use this file when
    you don't need to preprocess your data in any special way
"""

# import the abstract dataset classes
import dataset.abstract_dataset as abs_data

# opencv stuff for images and numpy for matrix representation
import cv2

# file and folder handling
import os
from os import listdir
from os.path import isfile, join
from colorama import Fore, Back, Style
import shutil
import scipy.misc as scm
import numpy as np
from skimage import transform

random_seed=0
np.random.seed(random_seed)
import random
random.seed(random_seed)

import sys

import dataset.utils as utils

def dir_to_data(directory, label_map, label_remap, new_shape=None, force_remap=False, cut_size=10000):
  """Get the dataset in the format that we need it
     and give it to dataset generator
  Args:
    directory: Folder where the dataset is stored
    label_map: all the classes we know
    label_remap: remap to classes we need for the crossentropy loss
    new_shape: New shape for the images to use
    force_remap: Delete remap folders, so that they are populated again
  Returns:
    images: images file names
    labels: remaped label filenames
    n_data = amount of data in the dataset
    content_perc = dict of percentage content for each class in the entire set
  """
  print("---------------------------------------------------------------------")
  print("Parsing directory %s" % directory)
  print("CUT--SIZE",cut_size)
  # make lists strings

  # create the content dictionary and other statistics in data
  content_perc = {}
  for key in label_map:
    content_perc[key] = 0.0
  total_pix = 0.0
  n_data = 0

  # image_path = directory+'/images/'    
  # annot_path = directory+'/dataset.txt'
  # #TODO 
  # gen_imgs_lbls(directory, image_path, annot_path)


  img_dir = directory + '/img/'
  img_remap_dir = img_dir + '/remap/'
  label_dir = directory + '/lbl/'
  label_remap_dir = label_dir + '/remap/'


  # get the file list in the folder
  images = [f for f in listdir(img_dir)
            if isfile(join(img_dir, f))]

  labels = [f for f in listdir(label_dir)
            if (isfile(join(label_dir, f)) and "pts.npy" not in f) ]

  #ptss = [f for f in listdir(label_dir)
  #          if (isfile(join(label_dir, f)) and "pts.npy" in f)]
  # IMPORANT!! sort the list!!! They are not in alphanumeric order!!!
  images.sort()
  labels.sort()
  #ptss.sort()


  N=len(images)
  # check if image has a corresponding label, otherwise warn and continue
  label_set=set([os.path.splitext(l)[0] for l in labels[:]])
  for f in images[:]:
    if os.path.splitext(f)[0] not in label_set:#[os.path.splitext(l)[0] for l in labels[:]]:
      # warn
      print("Image file %s has no label, GIMME DAT LABEL YO! Ignoring..." % f)
      # ignore image
      images.remove(f)
    else:
      # success!
      n_data += 1
      if n_data>=cut_size:
        print("Online cut data size to",n_data)
        break
      
      # calculate class content in the image
      # print("Calculating label content of label %s"%f)
      # TODO need to merge somehow? (maybe not, since this is not base class) 
      # l = cv2.imread(label_dir + os.path.splitext(f)[0] + ".png", 0)  # open label as grayscale
      """
      l = np.load(label_dir + os.path.splitext(f)[0] + ".npy")
      h, w = l.shape
      total_pix += h * w  # add to the total count of pixels in images
      # print("Number of pixels in image %s"%(h*w))
      # create histogram
      hist = cv2.calcHist([l], [0], None, [256], [0, 256])
      for key in content_perc:
        # look known class
        content_perc[key] += hist[key]
        hist[key] = 0
        # print("Content of class %s: %f"%(label_map[key],content_perc[key]))
      # report unknowkn class
      flag = 0
      for i in range(0, 256):
        if hist[i] > 0:
          if not flag:
            print(Back.RED + "Achtung! Img %s" % f + Style.RESET_ALL)
            flag = 1
          print(Fore.RED + "   ├─── labels contains %d unmapped class pixels with value %d" %
                (hist[i], i) + Style.RESET_ALL)
      if flag:
        # there were pixels that don't belong to our classes, so drop before
        # breaking our crossentropy
        print("Dropping image %s" % f)
        labels.remove(os.path.splitext(f)[0] + ".png")
        images.remove(f)
      """
  """
  # loop labels checking rogue labels with no images (magic label from ether)
  for f in labels[:]:
    if os.path.splitext(f)[0] not in [os.path.splitext(i)[0] for i in images[:]]:
      # warn
      print("Label file %s has no image, IS THIS MAGIC?! Ignoring..." % f)
      # ignore image
      labels.remove(f)
  """
  """
  # remove remap folders to create them again
  if force_remap and os.path.exists(label_remap_dir):
    shutil.rmtree(label_remap_dir)
  if force_remap and os.path.exists(img_remap_dir):
    shutil.rmtree(img_remap_dir)

  # remap all labels to [0,num_classes], otherwise it breaks the crossentropy
  if not os.path.exists(label_remap_dir):
    print("Cross Entropy remap non existent, creating...")
    os.makedirs(label_remap_dir)
    for f in labels:
      lbl = cv2.imread(label_dir + f, 0)
      if(new_shape is not None):
        lbl = cv2.resize(lbl, new_shape, interpolation=cv2.INTER_NEAREST)
      for key in label_remap:
        lbl[lbl == key] = label_remap[key]
      cv2.imwrite(directory + '/lbl/remap/' + f, lbl)

  # remap all images to jpg and resized to proper size, so that we open faster
  new_images = []
  if not os.path.exists(img_remap_dir):
    print("Jpeg remap non existent, creating...")
    os.makedirs(img_remap_dir)
    for f in images:
      img = cv2.imread(img_dir + f, cv2.IMREAD_UNCHANGED)
      f = os.path.splitext(f)[0] + '.jpg'
      new_images.append(f)
      if(new_shape is not None):
        img = cv2.resize(img, new_shape, interpolation=cv2.INTER_LINEAR)
      cv2.imwrite(img_remap_dir + f, img)
  else:
    for f in images:
      f = os.path.splitext(f)[0] + '.jpg'
      new_images.append(f)
  """
  """
  # final percentage calculation
  print("Total number of pixels: %d" % (total_pix))
  for key in content_perc:
    print("Total number of pixels of class %s: %d" %
          (label_map[key], content_perc[key]))
    content_perc[key] /= total_pix
  # report number of class labels
  for key in label_map:
    print("Content percentage of class %s in dataset: %f" %
          (label_map[key], content_perc[key]))
  """

  print("Total amount of images: %d" % n_data)
  #print("HG-IMAGES:")  #TODO remove it 
  #print(images)
  #print("HG-LABLES:")
  #print(labels)
  print("---------------------------------------------------------------------")

  # prepend the folder to each file name
  #TODO do we need remap? I don't think so...
  """
  new_images = [directory + '/img/remap/' + name for name in new_images]
  labels = [directory + '/lbl/remap/' + name for name in labels]

  # order to ensure matching (necessary?)
  assert(len(new_images) == len(labels))
  new_images.sort()
  labels.sort()

  return new_images, labels, n_data, content_perc
  """
  #print("DIR",directory)
  #c=input()



  #TODO just for debuging

  use_fixed_file=False

  if use_fixed_file:
    name="n03770679_13041A"#"n06277280_53768A"
    if "train" in directory:
      images=[name+".jpg"]*4
      labels=[name+".npy"]*4
      #ptss=[name+"pts.npy"]*4
  #  #images=["2008_000021A.jpg","2008_000015A.jpg","2008_000016A.jpg","2008_000023B.jpg"]
  #  #labels=["2008_000021A.npy","2008_000015A.npy","2008_000016A.npy","2008_000023B.npy"]





  #TODO oh my god, without slicing, cut_size doesn't work...(2018-07-08)
  images=[ directory + '/img/' + name for name in images ][:cut_size]
  labels=[ directory + '/lbl/' + name for name in labels ][:cut_size]
  #ptss=[ directory + '/lbl/' + name for name in ptss ][:cut_size]  
  #print("IMAGE",images)
  #print("LABEL",labels)
  #print("DEBUG:",len(images),len(labels))
  #for i in range(10):
  #  print("DEBUG:",images[i],labels[i])
  #c=input()
  return images,labels,n_data,content_perc


def read_data_sets_obsolete(DATA):
  """Get the dataset in the format that we need it
     and give it to the main app
  -*- coding: utf-8 -*-
  Args:
    DATA: Dictionary with dataset information.
          Structure for the input dir:
            Dataset
              ├── test
              │   ├── img
              │   │   └── pic3.jpg
              │   └── lbl
              │       └── pic3.jpg
              ├── train
              │   ├── img
              │   │   └── pic1.jpg
              │   └── lbl
              │       └── pic1.jpg
              └── valid
                  ├── img
                  │   └── pic2.jpg
                  └── lbl
                      └── pic2.jpg
  Returns:
    data_sets: Object of class dataset where all training, validation and
               test data is stored, along with other information to train
               the desired net.
  """

  # get the datasets from the folders
  data_dir = DATA['data_dir']

  directories = ["/train", "/valid", "/test"]
  types = ["/img", "/lbl"]

  # check that all folders exist:
  for d in directories:
    for t in types:
      if not os.path.exists(data_dir + d + t):
        print("%s dir does not exist. Check dataset folder" %
              (data_dir + d + t))
        quit()

  print("Data depth: ", DATA["img_prop"]["depth"])

  # if force resize, do it properly
  if "force_resize" in DATA and DATA["force_resize"]:
    new_rows = DATA["img_prop"]["height"]
    new_cols = DATA["img_prop"]["width"]
    new_shape = (new_cols, new_rows)
  else:
    new_shape = None

  # for backward compatibility
  if "force_remap" in DATA:
    force_remap = DATA["force_remap"]
  else:
    force_remap = False

  # train data
  train_img, train_lbl, train_n, train_cont = dir_to_data(join(data_dir, "train"),
                                                          DATA["label_map"],
                                                          DATA["label_remap"],
                                                          new_shape=new_shape,
                                                          force_remap=force_remap)
  train_data = abs_data.Dataset(train_img, train_lbl, train_n, train_cont,
                                "train", DATA)

  # validation data
  valid_img, valid_lbl, valid_n, valid_cont = dir_to_data(join(data_dir, "valid"),
                                                          DATA["label_map"],
                                                          DATA["label_remap"],
                                                          new_shape=new_shape,
                                                          force_remap=force_remap)
  valid_data = abs_data.Dataset(valid_img, valid_lbl, valid_n, valid_cont,
                                "valid", DATA)

  # test data
  test_img, test_lbl, test_n, test_cont = dir_to_data(join(data_dir, "test"),
                                                      DATA["label_map"],
                                                      DATA["label_remap"],
                                                      new_shape=new_shape,
                                                      force_remap=force_remap)
  test_data = abs_data.Dataset(test_img, test_lbl, test_n, test_cont,
                               "test", DATA)

  data_sets = abs_data.FullDataset(train_data, valid_data, test_data, DATA)

  print("Successfully imported datasets")
  print("Train data samples: ", train_n)
  print("Validation data samples: ", valid_n)
  print("Test data samples: ", test_n)

  return data_sets


def read_data_sets(DATA):
  """Get the dataset in the format that we need it
     and give it to the main app
  -*- coding: utf-8 -*-
  
  Procedures:
  1. read from images and annot_file
  2. move unlabel ones all into train part
  3. generate gaussian heatmaps
  4. save to followed dir structure
            Dataset
              ├── test
              │   ├── img
              │   │   └── pic3.jpg
              │   └── lbl
              │       └── pic3.jpg (for vis only)
              |       └── pic3.npy (for real lbl)
              ├── train
              │   ├── img
              │   │   └── pic1.jpg
              │   └── lbl
              │       └── pic1.jpg
              |       └── pic1.npy
              └── valid
                  ├── img
                  │   └── pic2.jpg
                  └── lbl
                      └── pic2.jpg    
                      └── pic2.npy
  # rm and mkdir test/ train/ valid/

  # mkdir img lbl under each sub folder test/train/valid/

  # 

  Returns:
    data_sets: Object of class dataset where all training, validation and
               test data is stored, along with other information to train
               the desired net.
  """

  # get the datasets from the folders
  data_dir = DATA['data_dir']

  directories = ["/train", "/valid", "/test"]
  types = ["/img", "/lbl"]

  # if force resize, do it properly
  if "force_resize" in DATA and DATA["force_resize"]:
    new_rows = DATA["img_prop"]["height"]
    new_cols = DATA["img_prop"]["width"]
    new_shape = (new_cols, new_rows)
  else:
    new_shape = None

  # for backward compatibility
  if "force_remap" in DATA:
    force_remap = DATA["force_remap"]
  else:
    force_remap = False



  gen_images=DATA["gen_images"]
  print("gen_images=",gen_images)

  if gen_images==True:

    # check that all folders exist:
    # 1. replace this with rm those folders and create again
    for d in directories:
      for t in types:
        if os.path.exists(data_dir + d + t):
          shutil.rmtree(data_dir+d+t)
          #print("%s dir does not exist. Check dataset folder" %
          #      (data_dir + d + t))
        os.makedirs(data_dir+d+t,exist_ok=True)
    print("Data depth: ", DATA["img_prop"]["depth"])


    # 2. generate those labels and devide those images into diff subfolders 
    # After this we should have three types of files in each of three folders
    # For valid set, we should only put all labelled dataset there
    # Don't need to save any structure in dictionary, just save to files
    annot_file = open(data_dir+"/dataset.txt", 'r')
    
    isFullyLabelled={}

    allNameList=[]
    fullyLabelledList=[]
    
    

    ## First we generate heatmap and put them all into trainset
    ## and maintain a list for all the filename and a dict to check its status
    ## state for isFullyLabelled. True/False
    for idx,line in enumerate(annot_file):
      line = line.strip()
      line = line.split(' ')
      name = line[0]
      if DATA["auto_crop"]:
        box = list(map(int,line[1:5]))
        joints = list(map(int,line[5:]))
      else:
        box = None
        joints = list(map(float,line[3].split(",")))
        scale = float(line[1])
        center = list(map(float,line[2].split(",")))
      
      if idx>=DATA["total_size"]:
        print("Cutting size to",idx)
        break
      
      if valid_joint_set(joints):
        
        if len(name.split(".jpg")[1])==0:
          continue

        print(data_dir+"/images/"+name)
        img=open_img(data_dir+"/images/"+name)

        # xxx.jpga -> xxxa.jpg (right case)
        # xxx.jpg -> xxx.jpg (skip this case)
        name="".join(name.split(".jpg")+[".jpg"])
        #name=name[:-5]+name[-1]+name[-5:-1]

        allNameList.append(name)
        if -1 not in joints or DATA["auto_crop"]==False:
          fullyLabelledList.append(name)

        # very thin label vector
        joints = np.reshape(joints, (-1,2))
        
        # this for weight
        w = [1] * joints.shape[0]
        for i in range(joints.shape[0]):
          if joints[i][0]<0:
            w[i] = 0
        #print(name,"weight",w)
        weight = w
        
        if DATA["auto_crop"]: # mpii style, work for human-pose
          padd, cbox = _crop_data(img.shape[0], img.shape[1], box, joints, boxp = 0.2)
          new_j = _relative_joints(cbox,padd, joints, to_size=64)
          img = _crop_img(img, padd, cbox)
          img = img.astype(np.uint8)
          # On 16 image per batch
          # Avg Time -OpenCV : 1.0 s -skimage: 1.25 s -scipy.misc.imresize: 1.05s
          img = scm.imresize(img, (256,256))
        else: # pascal style, work for multi-class
          new_j = _trans_joints(joints, scale, center,img.shape[1],img.shape[0],name)
          img = _trans_img(img, scale, center)
          img = img.astype(np.uint8)
          # On 16 image per batch
          # Avg Time -OpenCV : 1.0 s -skimage: 1.25 s -scipy.misc.imresize: 1.05s
          img = scm.imresize(img, (256,256))
        
        #TODO what this param max_length=64 for ?
        ## Okay, weight is nothing. Don't need to give special treat in training loss
        ## Just put its heatmap to all zero. That's very easy
        hm = utils._generate_hm(64, 64, new_j, 64, weight)

        # Less efficient that OpenCV resize method
        #img = transform.resize(img, (256,256), preserve_range = True, mode = 'constant')
        # May Cause trouble, bug in OpenCV imgwrap.cpp:3229
        # error: (-215) ssize.area() > 0 in function cv::resize
        #img = cv2.resize(img, (256,256), interpolation = cv2.INTER_CUBIC)
        if DATA["augment"]:
          img, hm = _augment(img, hm)
        
        #hm = np.expand_dims(hm, axis = 0)
        #hm = np.repeat(hm, stacks, axis = 0)
        #train_img[i] = img.astype(np.float32) / 255
        #train_gtmap[i] = hm

        cv2.imwrite(data_dir+"/train/"+name,img)
        cv2.imwrite(data_dir+"/train/"+name.split(".")[0]+"hm.jpg",255*max_pick(hm))
        np.save(data_dir+"/train/"+name.split(".")[0]+".npy" ,hm)
        np.save(data_dir+"/train/"+name.split(".")[0]+"pts.npy",new_j)


    ## Then we need to divide for all the fully labelled part and move some to the 
    ## valid/test part (let's say ratio approx 8:1:1)
    ## okay, we need to do math first to decide how much to pick from fully-labelled samples
    train_ratio=DATA["train_ratio"] #0.6
    valid_ratio=DATA["valid_ratio"] #0.2
    test_ratio= DATA["test_ratio"]  #0.2
    
    random.shuffle(fullyLabelledList)
    m=len(fullyLabelledList)
    N=len(allNameList)
    print("Full:",m,"All:",N,"thres:",(int)((1-train_ratio)*N))
    if (1-train_ratio)*N<2 or m<2:
      print("too little samples")
      sys.exit("Right now the code just aborts here...too little samples!")
    
    
    # fully        |   unfully      | // case 0
    # fully | unfully               | // case 1
    #valid|test |      train        | // ideal 
    #xxxxx|xxxxx|xxxxxxxxxxxxxxxxxxx| // ideal
    
    valid_start=0
    test_start=0
    train_start=0
    
    if m>=(int)((1-train_ratio)*N):
      # just 0.1*N to valid, 0.1*N to test, else m-0.2*N to train
      test_start=(int)(valid_ratio*N)
      train_start=(int)((1-train_ratio)*N)
    else:
      # just equal split the rest and then put to valid and test respectively
      test_start = (int)(m/2)
      train_start = m

    restOfList=[x for x in allNameList if x not in fullyLabelledList[0:train_start]]
    
    for valid_name in fullyLabelledList[valid_start:test_start]:
      shutil.move(data_dir + "/train/"     + valid_name,                          \
                  data_dir + "/valid/img/" + valid_name                           )
      shutil.move(data_dir + "/train/"     + valid_name.split(".")[0] + "hm.jpg", \
                  data_dir + "/valid/"     + valid_name.split(".")[0] + "hm.jpg"  )
      shutil.move(data_dir + "/train/"     + valid_name.split(".")[0] + ".npy",   \
                  data_dir + "/valid/lbl/" + valid_name.split(".")[0] + ".npy"    )
      shutil.move(data_dir + "/train/"     + valid_name.split(".")[0] + "pts.npy",   \
                  data_dir + "/valid/lbl/" + valid_name.split(".")[0] + "pts.npy"    )

    for test_name in fullyLabelledList[test_start:train_start]:
      shutil.move(data_dir + "/train/"     + test_name,                           \
                  data_dir + "/test/img/"  + test_name                            )
      shutil.move(data_dir + "/train/"     + test_name.split(".")[0] + "hm.jpg",  \
                  data_dir + "/test/"      + test_name.split(".")[0] + "hm.jpg"   )
      shutil.move(data_dir + "/train/"     + test_name.split(".")[0] + ".npy",    \
                  data_dir + "/test/lbl/"  + test_name.split(".")[0] + ".npy"     )
      shutil.move(data_dir + "/train/"     + test_name.split(".")[0] + "pts.npy",    \
                  data_dir + "/test/lbl/"  + test_name.split(".")[0] + "pts.npy"     )

    for train_name in restOfList: #fullyLabelledList[train_start:]:
      shutil.move(data_dir + "/train/"     + train_name,                          \
                  data_dir + "/train/img/" + train_name                           )
      shutil.move(data_dir + "/train/"     + train_name.split(".")[0] + ".npy",   \
                  data_dir + "/train/lbl/" + train_name.split(".")[0] + ".npy"    )
      shutil.move(data_dir + "/train/"     + train_name.split(".")[0] + "pts.npy",   \
                  data_dir + "/train/lbl/" + train_name.split(".")[0] + "pts.npy"    )
      #shutil.move(data_dir + "/train/"     + train_name.split(".")[0] + "hm.jpg", \
      #            data_dir + "/train/"     + train_name.split(".")[0] + "hm.jpg"  )

    

  
  # 3. substitute the abstract_dataset part because labels in .npy format

  # sys.exit("Right now the code just aborts here...")

  # train data
  train_img, train_lbl, train_n, train_cont = dir_to_data(join(data_dir, "train"),
                                                          DATA["label_map"],
                                                          DATA["label_remap"],
                                                          new_shape=new_shape,
                                                          force_remap=force_remap, cut_size=DATA["cut_train_size"])
  train_data = abs_data.Dataset(train_img, train_lbl, train_n, train_cont,
                                "train", DATA)

  # validation data
  valid_img, valid_lbl, valid_n, valid_cont = dir_to_data(join(data_dir, "valid"),
                                                          DATA["label_map"],
                                                          DATA["label_remap"],
                                                          new_shape=new_shape,
                                                          force_remap=force_remap, cut_size=DATA["cut_valid_size"])
  valid_data = abs_data.Dataset(valid_img, valid_lbl, valid_n, valid_cont,
                                "valid", DATA)

  # test data
  test_img, test_lbl, test_n, test_cont = dir_to_data(join(data_dir, "test"),
                                                      DATA["label_map"],
                                                      DATA["label_remap"],
                                                      new_shape=new_shape,
                                                      force_remap=force_remap, cut_size=DATA["cut_test_size"])
  test_data = abs_data.Dataset(test_img, test_lbl, test_n, test_cont,
                               "test", DATA)

  data_sets = abs_data.FullDataset(train_data, valid_data, test_data, DATA)

  print("Successfully imported datasets")
  print("Train data samples: ", train_n)
  print("Validation data samples: ", valid_n)
  print("Test data samples: ", test_n)

  return data_sets

def max_pick(heatmap):
  h=np.shape(heatmap)[0]
  w=np.shape(heatmap)[1]
  k=np.shape(heatmap)[2]
  maxmap=np.zeros((np.shape(heatmap)[0],np.shape(heatmap)[1]))
  for i in range(k):
    max_index=np.argmax(heatmap[:,:,i])
    max_h=(int)(max_index/h) 
    max_w=max_index%w
    maxmap[max_h,max_w]=1
  return maxmap


def _crop_data(height, width, box, joints, boxp = 0.05):
  """ Automatically returns a padding vector and a bounding box given
  the size of the image and a list of joints.
  Args:
    height		: Original Height
    width		: Original Width
    box			: Bounding Box
    joints		: Array of joints
    boxp		: Box percentage (Use 20% to get a good bounding box)
  """
  padding = [[0,0],[0,0],[0,0]]
  j = np.copy(joints)
  if box[0:2] == [-1,-1]:
    j[joints == -1] = 1e5
    box[0], box[1] = min(j[:,0]), min(j[:,1])
  crop_box = [box[0] - int(boxp * (box[2]-box[0])), box[1] - int(boxp * (box[3]-box[1])), box[2] + int(boxp * (box[2]-box[0])), box[3] + int(boxp * (box[3]-box[1]))]
  if crop_box[0] < 0: crop_box[0] = 0
  if crop_box[1] < 0: crop_box[1] = 0
  if crop_box[2] > width -1: crop_box[2] = width -1
  if crop_box[3] > height -1: crop_box[3] = height -1
  new_h = int(crop_box[3] - crop_box[1])
  new_w = int(crop_box[2] - crop_box[0])
  crop_box = [crop_box[0] + new_w //2, crop_box[1] + new_h //2, new_w, new_h]
  if new_h > new_w:
    bounds = (crop_box[0] - new_h //2, crop_box[0] + new_h //2)
    if bounds[0] < 0:
      padding[1][0] = abs(bounds[0])
    if bounds[1] > width - 1:
      padding[1][1] = abs(width - bounds[1])
  elif new_h < new_w:
    bounds = (crop_box[1] - new_w //2, crop_box[1] + new_w //2)
    if bounds[0] < 0:
      padding[0][0] = abs(bounds[0])
    if bounds[1] > width - 1:
      padding[0][1] = abs(height - bounds[1])
  crop_box[0] += padding[1][0]
  crop_box[1] += padding[0][0]
  return padding, crop_box
	
def _crop_img(img, padding, crop_box):
  """ Given a bounding box and padding values return cropped image
  Args:
    img			: Source Image
    padding	: Padding
    crop_box	: Bounding Box
  """
  img = np.pad(img, padding, mode = 'constant')
  max_lenght = max(crop_box[2], crop_box[3])
  img = img[crop_box[1] - max_lenght //2:crop_box[1] + max_lenght //2, crop_box[0] - max_lenght // 2:crop_box[0] + max_lenght //2]
  return img
  
def _crop(img, hm, padding, crop_box):
  """ Given a bounding box and padding values return cropped image and heatmap
  Args:
    img			: Source Image
    hm			: Source Heat Map
    padding	: Padding
    crop_box	: Bounding Box
  """
  img = np.pad(img, padding, mode = 'constant')
  hm = np.pad(hm, padding, mode = 'constant')
  max_lenght = max(crop_box[2], crop_box[3])
  img = img[crop_box[1] - max_lenght //2:crop_box[1] + max_lenght //2, crop_box[0] - max_lenght // 2:crop_box[0] + max_lenght //2]
  hm = hm[crop_box[1] - max_lenght //2:crop_box[1] + max_lenght//2, crop_box[0] - max_lenght // 2:crop_box[0] + max_lenght // 2]
  return img, hm

def _relative_joints(box, padding, joints, to_size = 64):
  """ Convert Absolute joint coordinates to crop box relative joint coordinates
  (Used to compute Heat Maps)
  Args:
    box			: Bounding Box 
    padding	: Padding Added to the original Image
    to_size	: Heat Map wanted Size
  """
  new_j = np.copy(joints)
  max_l = max(box[2], box[3])
  new_j = new_j + [padding[1][0], padding[0][0]]
  new_j = new_j - [box[0] - max_l //2,box[1] - max_l //2]
  new_j = new_j * to_size / (max_l + 0.0000001)
  return new_j.astype(np.int32)

# joints and center are all in (x,y) coords 
def _trans_joints(joints,scale,center,width,height,name):
  new_joints=[]
  #print(joints)
  for i in range(joints.shape[0]):
    if joints[i,0]<0:
      new_joints.append(-1)
      new_joints.append(-1)
    else:
      #half_length=int(100*scale)
      #x=max(min(joints[i*2],center[0]+half_length),center[0]-half_length)
      #y=max(min(joints[i*2+1],center[1]+half_length),center[1]+half_length)
      
      x=joints[i,0]
      y=joints[i,1]

      x=(x-center[0])/(200*scale)*63+31.5
      y=(y-center[1])/(200*scale)*63+31.5
      
      x=max(min(int(x),63),0)
      y=max(min(int(y),63),0)

      new_joints.append(x)
      new_joints.append(y)
      if "n04331277_70" in name:
        print(x,y)
  return np.reshape(new_joints, (-1,2))

# find the bbx region img in the origin img
# if outside, using padding 
def _trans_img(img, scale, center):
  new_img=None
  #TODO maybe very slow now, optimize this part later
  length=(int)(200*scale)
  new_img=np.zeros((length,length,3))
  ul_x=int(center[0]-length/2)
  ul_y=int(center[1]-length/2)
  dr_x=length+ul_x
  dr_y=length+ul_y
  
  b_ul_x=max(0,ul_x)
  b_ul_y=max(0,ul_y)
  b_dr_x=min(img.shape[1],dr_x)
  b_dr_y=min(img.shape[0],dr_y)
  
  d_ul_x = b_ul_x - ul_x
  d_ul_y = b_ul_y - ul_y
  d_dr_x = dr_x - b_dr_x
  d_dr_y = dr_y - b_dr_y

  new_img[d_ul_y:length-d_dr_y,d_ul_x:length-d_dr_x,:]=img[b_ul_y:b_dr_y,b_ul_x:b_dr_x,:]
  return new_img


def _augment(img, hm, max_rotation = 30):
  """ # TODO : IMPLEMENT DATA AUGMENTATION 
  """
  if np.random.choice([0,1]): 
    r_angle = np.random.randint(-1*max_rotation, max_rotation)
    img = 	transform.rotate(img, r_angle, preserve_range = True)
    hm = transform.rotate(hm, r_angle)
  return img, hm


# def _generate_hm(height, width ,joints, maxlenght, weight):
#   """ Generate a full Heap Map for every joints in an array
#   Args:
#     height			: Wanted Height for the Heat Map
#     width			: Wanted Width for the Heat Map
#     joints			: Array of Joints
#     maxlenght		: Lenght of the Bounding Box
#   """
#   num_joints = joints.shape[0]
#   hm = np.zeros((height, width, num_joints), dtype = np.float32)
#   for i in range(num_joints):
#     if not(np.array_equal(joints[i], [-1,-1])) and weight[i] == 1:
#       hm[:,:,i] = _makeGaussian(height, width, center= (int(joints[i,0]), int(joints[i,1])))
#     else:
#       hm[:,:,i] = np.zeros((height,width))
#   return hm

def open_img(name, color = 'RGB'):
  """ Open an image 
  Args:
    name	: Name of the sample
    color	: Color Mode (RGB/BGR/GRAY)
  """
  #TODO check later
  #if name[-1] in self.letter:
  #  name = name[:-1]
  img = cv2.imread(name.split(".jpg")[0]+".jpg")
  if color == 'RGB':
    #TODO img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img
  elif color == 'BGR':
    return img
  # elif color == 'GRAY':
  #   img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  else:
    print('Color mode supported: RGB/BGR. If you need another mode do it yourself :p')

def valid_joint_set(joints):
  for val in joints:
    if val>=0:
      return True
  return False

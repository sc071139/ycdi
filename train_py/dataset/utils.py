import cv2
import numpy as np
import skimage.transform

import matplotlib.pyplot as plt
import time
scaleFactor=0.25
rotFactor=30


matchedParts = [\
[1,4], [2,5], [10,14], [11,15], [12,16], [13,17], [22,23], \
[24,25], [29,30], [32,33], [34,36], [35,37], [38,39], [40,41],\
[42,44], [43,45], [46,48], [47,49], [50,51], [52,53], [54,55],\
[56,57], [58,59], [60,61], [62,63], [64,65], [66,67], [68,69],\
[70,71], [72,73], [74,75], [76,77], [78,80], [79,81], [83,85],\
[84,86], [87,89], [88,90], [91,93], [92,94], [95,96], [97,98],\
[99,100], [101,102]]

d1={key-1:value-1 for (key,value) in matchedParts}

matchedDict=d1.copy()
#matchedDict.update(d2)
#for key in matchedDict:
#    print(key,matchedDict[key])

def clamp(val,minv,maxv):
    return np.clip(val, minv, maxv)

def _aug_on_fly(img,ptss):
    #TODO to see how they implement this augmentation in torch code!
    weight=(ptss[:,0]>-0.5).astype(int)
    
    #t1=time.time()
    lbl=_generate_hm(64,64,ptss,64,None)
    #t2=time.time()

    #color for each channel
    color_scale=np.random.uniform(0.8, 1.2, 3)
    for i in range(3):
        img[:,:,i]=clamp(img[:,:,i]*color_scale[i], 0, 1)
    #t3=time.time()

    # scale/rotation
    sr=np.random.randn(2,1)
    s=clamp(sr[0]*scaleFactor+1,1-scaleFactor,1+scaleFactor)
    r=clamp(sr[1]*rotFactor,-2*rotFactor,2*rotFactor)
    if np.random.uniform()<0.6:
      r[0]=0
    img=crop(img, [127.5,127.5], 256*s[0]/200, 256, r[0])
    lbl=crop(lbl, [31.5,31.5], 64*s[0]/200, 64, r[0])
    #t4=time.time()

    # flip
    if np.random.rand()<.5:
        img = np.flip(img,axis=1)
        lbl = np.flip(lbl,axis=1)

        for i,val_i in enumerate(weight):
            if i not in matchedDict:
                continue
            j=matchedDict[i]
            val_j=weight[j]
            if val_i>0.5 or val_j>0.5:
                tmp=np.array(lbl[:,:,i])
                lbl[:,:,i]=lbl[:,:,j]
                lbl[:,:,j]=tmp


    #t5=time.time()
    #print("genhm %2.2f  color %2.2f  scale %2.2f  flip %2.2f"%
    #((t2-t1)*1000,(t3-t2)*1000,(t4-t3)*1000,(t5-t4)*1000))
    return img,lbl


def _generate_hm(height, width ,joints, maxlenght, weight):
  """ Generate a full Heap Map for every joints in an array
  Args:
    height			: Wanted Height for the Heat Map
    width			: Wanted Width for the Heat Map
    joints			: Array of Joints
    maxlenght		: Lenght of the Bounding Box
  """
  num_joints = joints.shape[0]
  hm = np.zeros((height, width, num_joints), dtype = np.float32)
  for i in range(num_joints):
    if joints[i,0]>=-0.5:
      hm[:,:,i] = _makeGaussian(height, width, center= (int(joints[i,0]), int(joints[i,1])))
    else:
      hm[:,:,i] = np.zeros((height,width))
  return hm




def _makeGaussian(height, width, center):
  """ Make a square gaussian kernel (square means bound is square)
  """
  x = np.arange(0, width, 1, float)
  y = np.arange(0, height, 1, float)[:, np.newaxis]
  sigma=1.75 # after very precised math calculation from pavlakos code
  half=3 # dont ask, just this
  
  x0 = center[0]
  y0 = center[1]
  m = np.exp(-((x-x0)**2+(y-y0)**2)/2/sigma**2)
  
  leftx=max(0,x0-half)
  rightx=min(width,x0+half)
  topy=max(0,y0-half)
  downy=min(height,y0+half)
  
  cropped_m=np.zeros((height,width))
  cropped_m[topy:downy+1,leftx:rightx+1]=m[topy:downy+1,leftx:rightx+1]

  return cropped_m


def get_transform(center, scale, res, rot=0):
    # Generate transformation matrix
    h = 200 * scale
    t = np.zeros((3, 3))
    t[0, 0] = float(res) / h
    t[1, 1] = float(res) / h
    t[0, 2] = res * (-float(center[0]) / h + .5)
    t[1, 2] = res * (-float(center[1]) / h + .5)
    t[2, 2] = 1
    if not rot == 0:
        rot = -rot # To match direction of rotation from cropping
        rot_mat = np.zeros((3,3))
        rot_rad = rot * np.pi / 180
        sn,cs = np.sin(rot_rad), np.cos(rot_rad)
        rot_mat[0,:2] = [cs, -sn]
        rot_mat[1,:2] = [sn, cs]
        rot_mat[2,2] = 1
        # Need to rotate around center
        t_mat = np.eye(3)
        t_mat[0,2] = -res/2
        t_mat[1,2] = -res/2
        t_inv = t_mat.copy()
        t_inv[:2,2] *= -1
        t = np.dot(t_inv,np.dot(rot_mat,np.dot(t_mat,t)))
    return t

def transform(pt, center, scale, res, invert=0, rot=0):
    # Transform pixel location to different reference
    t = get_transform(center, scale, res, rot=rot)
    if invert:
        t = np.linalg.inv(t)
    new_pt = np.array([pt[0], pt[1], 1.]).T
    new_pt = np.dot(t, new_pt)
    return new_pt[:2].astype(int)

def crop(img, center, scale, res, rot=0):
    # Upper left point
    ul = np.array(transform([0, 0], center, scale, res, invert=1))
    # Bottom right point
    br = np.array(transform([res,res], center, scale, res, invert=1))

    # Padding so that when rotated proper amount of context is included
    pad = int(np.linalg.norm(br - ul) / 2 - float(br[1] - ul[1]) / 2)
    if not rot == 0:
        ul -= pad
        br += pad

    new_shape = [br[1] - ul[1], br[0] - ul[0]]
    if len(img.shape) > 2:
        new_shape += [img.shape[2]]
    new_img = np.zeros(new_shape)

    # Range to fill new array
    new_x = max(0, -ul[0]), min(br[0], len(img[0])) - ul[0]
    new_y = max(0, -ul[1]), min(br[1], len(img)) - ul[1]
    # Range to sample from original image
    old_x = max(0, ul[0]), min(len(img[0]), br[0])
    old_y = max(0, ul[1]), min(len(img), br[1])
    new_img[new_y[0]:new_y[1], new_x[0]:new_x[1]] = img[old_y[0]:old_y[1], old_x[0]:old_x[1]]
    #plt.imshow(new_img)
    #plt.show()
    #t0=time.time()
    if not rot == 0:
        new_img=cv2_rotate(new_img,rot)
        #new_img=skimage.transform.rotate(new_img,rot)
        new_img=new_img[pad:-pad, pad:-pad]
    #t1=time.time()
    new_new_img=cv2.resize(new_img, (res, res), interpolation=cv2.INTER_LINEAR)
    #new_new_img= skimage.transform.resize(new_img,(res,res), mode='constant')
    #t2=time.time()
    #print("rot %2.2f scale %2.2f"%((t1-t0)*1000,(t2-t1)*1000))
    return new_new_img
        # Remove padding
    #    for i in range(np.shape(new_img)[2]):
    #        new_img[:,:,i] = scipy.misc.imrotate(new_img[:,:,i], rot)
    #    new_img = new_img[pad:-pad, pad:-pad]
    
    #for i in range(np.shape(new_img)[2]):
    #    new_new_img[:,:,i]=
    #return scipy.misc.imresize(new_img, res)

def cv2_rotate(image, angle):
  image_center = tuple(np.array(image.shape[1::-1]) / 2)
  rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
  result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
  return result

# def nms(img):
#     # Do non-maximum suppression on a 2D array
#     win_size = 3
#     domain = np.ones((win_size, win_size))
#     maxes = scipy.signal.order_filter(img, domain, win_size ** 2 - 1)
#     diff = maxes - img
#     result = img.copy()
#     result[diff > 0] = 0
#     return result


# def transform(pt, center, scale, rot, res, invert):




# def crop(img, center, scale, rot ,res):
#     ul=transform()

def vis_img(img,saved_path):
    img_plt=np.array(img)
    img_plt[:,:,0]=img[:,:,2]
    img_plt[:,:,2]=img[:,:,0]
    
    plt.imshow(img_plt)
    plt.savefig(saved_path)
    plt.close()

def vis_hm(hm, saved_path):
    mask=np.zeros((64,64))
    for i in range(np.shape(hm)[2]):
        idx=np.argmax(hm[:,:,i])
        x=idx%64
        y=idx//64
        mask[y,x]=i%10/10
    plt.imshow(mask)
    plt.savefig(saved_path)
    plt.close()

def test_utils():
    img_name="/home/meng/dataset/pascal3d+/data/pascal3d/dest/train/img/n04331277_6921A.jpg"
    ptss_name="/home/meng/dataset/pascal3d+/data/pascal3d/dest/train/lbl/n04331277_6921Apts.npy"
    n_times=10
    img = cv2.imread(img_name,cv2.IMREAD_COLOR)/255
    ptss=np.load(ptss_name)
    for i in range(n_times):
        t0=time.time()
        img_new,lbl=_aug_on_fly(img,ptss)
        t1=time.time()
        print("total time",t1-t0)
        vis_img(img_new,"test/test_img"+str(i)+".png")
        vis_hm(lbl,"test/test_hm"+str(i)+".png")
    
#test_utils()

import h5py
import os
import numpy as  np
import matplotlib.pyplot as plt
from pathlib import Path
from shutil import copyfile



img_dir= "/home/meng/dataset/pascal3d+/data/pascal3d/images/"  # there are origin images
ann_dir= "/home/meng/dataset/pascal3d+/data/pascal3d/annot/"   # there are annot and name list
dst_dir= "/home/meng/dataset/pascal3d+/data/pascal3d/dest/"    # there will be a bunch of images and a rich note file
dst_img_dir=dst_dir+"images/"
if not os.path.exists(dst_dir):
    os.makedirs(dst_dir)
if not os.path.exists(dst_img_dir):
    os.makedirs(dst_img_dir)

name_counter={}
char_list=[chr(x+ord("A")) for x in range(26)]
txt_file_list=["train_images.txt","valid_images.txt"]
hdf_file_list=["train.h5","valid.h5"]
#print(char_list)
with open(dst_dir+"dataset.txt","w") as writer:
    for pair_id in range(2):
        txt_name=txt_file_list[pair_id]
        hdf_name=hdf_file_list[pair_id]
        with open(ann_dir+txt_name,"r") as f:
            hdf_f=h5py.File(ann_dir+hdf_name,"r")
            for line_i,line in enumerate(f.readlines()):
                image_name=line.strip()
                the_file = Path(img_dir+image_name)
                if the_file.exists():
                    print("I found", image_name)
                    if image_name not in name_counter:
                        name_counter[image_name]=0
                        # copy img to some place
                        copyfile(img_dir+image_name, dst_img_dir+image_name)
                    else:
                        name_counter[image_name]+=1
                    if name_counter[image_name]>=26:
                        continue
                    new_image_name=image_name+char_list[name_counter[image_name]]

                    # name scale center kpt_locs
                    #TODO keep value all in int or float?
                    dataline="{0} {1} {2} {3}".format(
                        new_image_name,
                        hdf_f["scale"][line_i],
                        ",".join([str(x) for x in hdf_f["center"][line_i]]),
                        ",".join([str(x) for x in hdf_f["part"][line_i].reshape(1,-1)[0]])
                    )
                    writer.write(dataline+"\n")

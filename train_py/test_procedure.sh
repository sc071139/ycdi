# This is a script telling how to test each steps
# The migration comes from several steps:
# If we name our exp_name as hourglass,
# 1. First we prepare the dataset (using ~/dataset/mpii_lite/foo/images)
# 2. Then we write python to grab the data (since our outputs are bunch of heatmaps, we cannot save it to jpg. We need to save it to numpy-related file and extract it back later. Also we don't need the augmentation but we need to generate gaussian heatmaps, so need to change in abstract_data)
# 3. Then we test this section

### in python ###
#	f = open(FLAGS.path + '/data.yaml', 'r')
#	DATA = yaml.load(f)
#	parser = imp.load_source("parser", os.getcwd() + '/dataset/' +
#                                  self.DATA["name"] + '.py')
#	parser.read_data_sets(DATA)
### check the output and in the folders

python3 test_dataset.py

# The next part is to modify the network structure and design for the train procedures....

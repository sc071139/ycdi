#!/usr/bin/python3

# Copyright 2017 Andres Milioto. All Rights Reserved.
#
#  This file is part of Bonnet.
#
#  Bonnet is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Bonnet is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Bonnet. If not, see <http://www.gnu.org/licenses/>.

'''
  Network class, containing definition of the graph
  API Style should be the same for all nets (Same class name and member functions)
'''
# tf
import tensorflow as tf

# common layers
from arch.abstract_net import AbstractNetwork
import arch.layer as lyr
import sys
import numpy as np
import os
import time
import imp
import yaml
import arch.msg as msg
import cv2
import matplotlib.pyplot as plt
plt.switch_backend('Agg')

import scipy.misc
from skimage.draw import line

from tensorflow.python.client import timeline
from tensorflow.python.tools import freeze_graph
from tensorflow.tools.graph_transforms import TransformGraph
from arch.my_rmsprop import MyRMSProp

index_list=[[0,1,2,3,4,5,6,7],
            [8,9,10,11,12,13,14,15,16,17],
            [18,19,20,21,22,23,24,25],
            [26,27,28,29,30,31,32],
            [33,34,35,36,37,38,39,40,41,42,43,44],
            [45,46,47,48,49,50,51,52,53,54,55,56],
            [57,58,59,60,61,62,63,64,65,66],
            [67,68,69,70,71,72,73,74,75,76],
            [77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93],
            [94,95,96,97,98,99,100,101]]

class_list=["aero ", "bike ", "boat ", "botle", "bus  ",
            "car  ", "chair", "sofa ", "train", "tvmon"]

class Network(AbstractNetwork):
  def __init__(self, DATA, NET, TRAIN, logdir):
    # init parent
    super().__init__(DATA, NET, TRAIN, logdir)

    self.first_time_do_this=True
    
    # TODO future config list (migrating from hgtf) 
    self.tiny=False
    self.modif=False
    self.stickness=self.TRAIN["stickness"]
    self.first_stick=True
    self.print_name=False # print file name in non-stickenss mode

    # net config
    self.nFeat=256
    self.nStack=2
    self.nLow=self.TRAIN["nLow"]
    
    self.outDim=DATA["img_prop"]["hm_depth"] # number of Joints
    self.dropout_rate=TRAIN["dropout_rate"]
    
    self.counter_dict={}
    self.train_loss_list=[]
    self.valid_loss_list=[]
    self.test_loss_list=[]
    self.train_acc_list=[]
    self.valid_acc_list=[]
    self.test_acc_list=[]
    self.acc_threshold=TRAIN["acc_threshold"]
    self.num_classes=len(self.DATA["label_map"])
    self.net_type=self.TRAIN["net_type"]
    self.df=self.TRAIN["data_format"]
    self.smart_conv=self.TRAIN["smart_conv"]
    #self.simple_net=self.TRAIN["simple_net"]
  
  def warning(self, word):
    length=len(word)
    print("." + "-" * length + ".")
    print("|" + word.upper() + "|")
    print("'" + "-" * length + "'")
    print()

  def build_graph(self,img_pl,train_stage,simple_net=False):
    
    print("Building graph")

    if self.net_type=="simple":
    #  return self.build_graph_light_linear(img_pl,train_stage)
    #  return self.build_graph_light_single(img_pl,train_stage)
    #  return self.build_graph_light_double(img_pl,train_stage)
      return self.build_graph_light_residual(img_pl,train_stage)
    elif self.net_type=="origin":
      return self.build_graph_full(img_pl,train_stage)
    else:
      self.warning("check your neural network type!")
      sys.exit()
      return None

  """ only one layer of linear opers

  [img_pl] -> normalize -> pad -> conv -> concat -> [n_logits] 
  """
  def build_graph_light_linear(self,img_pl,train_stage):
    #n_img=img_pl/255
    with tf.variable_scope('images'):
      if self.df == "NCHW":
        img_transposed = tf.transpose(img_pl, [0, 3, 1, 2])
      else:
        img_transposed = img_pl
    n_img=img_transposed
    self.obs_lin_0input=img_pl
    pad1=self._pad(n_img,15,name="pad_1")
    conv1=self._conv(train_stage,pad1,filters=102,kernel_size=31,strides=4,name="conv",_use_bias=True)
    #conv1=self._conv_bn_relu(train_stage,pad1,filters=102,kernel_size=31,strides=2,name="conv_256_102",data_format=data_format)

    logits=conv1
    if self.df=="NCHW":
      logits = tf.transpose(logits, [0, 2, 3, 1])
    n_logits=tf.concat([logits,logits],axis=3,name="final_output")
    self.obs_lin_output=logits
    return logits, n_logits, n_img

  """ single conv_bn_relu with pool

  [img_pl] => normalize => pad => conv_bn_relu => pool => concat => [n_logits]
  """
  def build_graph_light_single(self, img_pl, train_stage):
    with tf.variable_scope('images'):
      if self.df == "NCHW":
        img_transposed = tf.transpose(img_pl, [0, 3, 1, 2])
      else:
        img_transposed = img_pl
    n_img=(img_transposed-128)/128
    pad1=self._pad(n_img,3,name="pad_1")
    conv1=self._conv_bn_relu(train_stage, pad1, filters= 102, kernel_size = 7, strides = 2, name = 'conv_256_to_128')
    
    #sth = self._residual(train_stage, conv1, numOut= 128, name = 'rr', data_format=data_format)
    pool = self._max_pool2d(conv1, [2,2], [2,2], padding='VALID')

    #logits=self._hourglass(train_stage, pool, 1, 102, 'hourglass',data_format=data_format)
    logits=pool
    if self.df=="NCHW":
      logits = tf.transpose(logits, [0, 2, 3, 1])
    n_logits=tf.concat([logits,logits], axis= 3 , name = 'final_output')	

    return logits, n_logits, n_img

  """ double layer conv_bn_relu with pool
  
  [img_pl] => normalize => pad => conv_bn_relu => pool => pad => conv_bn_relu => concat => [n_logits]
  """
  def build_graph_light_double(self,img_pl,train_stage): # solve for batch init and rms decay
    with tf.variable_scope('images'):
      if self.df == "NCHW":
        img_transposed = tf.transpose(img_pl, [0, 3, 1, 2])
      else:
        img_transposed = img_pl
    #n_img=img_pl/255
    n_img=img_transposed
    #if train_stage:
    #  self.obs_1output_list.append(n_img)
    pad1=self._pad(n_img,9,name="pad_1")
    #conv1=nn.batch_norm(self._conv(train_stage,pad1,filters=256,kernel_size=19,strides=2,name="conv3_256",data_format=data_format, _use_bias=True))
    #conv1=tf.nn.relu(self._conv(train_stage,pad1,filters=256,kernel_size=19,strides=2,name="conv3_256",data_format=data_format, _use_bias=True))
    #conv1=self._bn(train_stage,self._conv(train_stage,pad1,filters=256,kernel_size=19,strides=2,name="conv3_256",data_format=data_format,_use_bias=True))
    #conv1=self._conv(train_stage,pad1,filters=256,kernel_size=19,strides=2,name="conv3_256",data_format=data_format, _use_bias=True)
    conv1=self._conv_bn_relu(train_stage,pad1,filters=256,kernel_size=19,strides=2,name="conv3_256")
    #if train_stage:
      #self.obs_4relu_list.append(conv1)
    pool = self._max_pool2d(conv1,[2,2],[2,2],padding="VALID")
    pad2=self._pad(pool,3,name="pad_2")
    #logits= tf.nn.relu(self._conv(train_stage,pad2,filters=102,kernel_size=7,strides=1,name="conv_256_102",data_format=data_format, _use_bias=True))
    #logits=self._bn(train_stage,self._conv(train_stage,pad2,filters=102,kernel_size=7,strides=1,name="conv_256_102",data_format=data_format,_use_bias=True))
    #logits=self._conv(train_stage,pad2,filters=102,kernel_size=7,strides=1,name="conv_256_102",data_format=data_format, _use_bias=True)
    logits=self._conv_bn_relu(train_stage,pad2,filters=102,kernel_size=7,strides=1,name="conv_256_102")
    #if train_stage:
    #  self.obs_8output_list.append(logits)
    if self.df=="NCHW":
      logits = tf.transpose(logits, [0, 2, 3, 1])
    n_logits=tf.concat([logits,logits],axis=3,name="final_output")
    return logits, n_logits, n_img

  """ with more complex model, residual block or even an hourglass there
     
  (img_pl) => TBA => (n_logits)
  """
  def build_graph_light_residual(self,img_pl,train_stage):

    test_hg=False

    with tf.variable_scope('images'):
      if self.df == "NCHW":
        img_transposed = tf.transpose(img_pl, [0, 3, 1, 2])
      else:
        img_transposed = img_pl
      #n_img=img_pl/255
      n_img=img_transposed
      print("n_img",n_img.get_shape())

    with tf.variable_scope("preprocessing"):
      pad1 = self._pad(n_img, 3, name='pad_1')

      conv1=self._conv_bn_relu(train_stage, pad1, filters= 64, kernel_size = 7, strides = 2, name = 'conv_256_to_128')
      # Dim conv1 : nbImages x 64 x 128 x 128

      r1 = self._residual(train_stage, conv1, numOut = 128, name = 'r1')
      # Dim r1 : nbImages x 128 x 128 x 128

      pool1 = self._max_pool2d(r1, [2,2], [2,2], padding='VALID')
      # Dim pool1 : nbImages x 128 x 64 x 64

      r4 = self._residual(train_stage, pool1, numOut= int(self.nFeat/2), name = 'r4')
      r5 = self._residual(train_stage, r4, numOut= int(self.nFeat/2), name = 'r5')
      if test_hg==False:
        logits = self._residual(train_stage, r5, numOut= self.outDim, name = 'r6')
      else:
        with tf.variable_scope('stage_0'): #tf.name_scope('stage_0'):
          r6 = self._residual(train_stage, r5, numOut=self.nFeat, name='r6')
          hg0 = self._hourglass(train_stage, r6, self.nLow, self.nFeat*2, 'hourglass')
          ll0 = self._conv_bn_relu(train_stage, hg0, self.nFeat*2, 1,1, 'VALID', name = 'conv')
          ll_0 =  self._conv_bn_relu(train_stage, ll0, self.nFeat, 1, 1, 'VALID', 'll')
          logits = self._conv(train_stage, ll_0, self.outDim, 1, 1, 'VALID', 'out',_use_bias=self.TRAIN["use_bias"])
    
    if self.df=="NCHW":
      logits = tf.transpose(logits, [0, 2, 3, 1])
    n_logits=tf.concat([logits,logits],axis=3,name="final_output")
    return logits, n_logits, n_img


  """ full power of stacked hourglass model
  """
  def build_graph_full(self, img_pl, train_stage):

    with tf.variable_scope('images'):

      if self.df == "NCHW":
        img_transposed = tf.transpose(img_pl, [0, 3, 1, 2])
      else:
        img_transposed = img_pl

      n_img=img_transposed
      #print("n_img",n_img.get_shape())

    with tf.variable_scope("preprocessing"):
      pad1 = self._pad(n_img, 3, name='pad_1')

      # Dim conv1 : nbImages x 64 x 128 x 128
      conv1=self._conv_bn_relu(train_stage, pad1, \
            filters= 64, kernel_size = 7, strides = 2, \
            name = 'conv_256_to_128')
      
      # Dim r1 : nbImages x 128 x 128 x 128
      r1 = self._residual(train_stage, conv1, numOut = 128, name = 'r1')
      
      # Dim pool1 : nbImages x 128 x 64 x 64
      pool1 = self._max_pool2d(r1, [2,2], [2,2], padding='VALID')
      
      
      #FIXME followed pavlakos's notation r456
      if self.tiny:
        r6 = self._residual(train_stage, pool1, numOut=self.nFeat, name='r6')
      else:
        r4 = self._residual(train_stage, pool1, numOut= int(self.nFeat/2), name = 'r4')
        r5 = self._residual(train_stage, r4, numOut= int(self.nFeat/2), name = 'r5')
        r6 = self._residual(train_stage, r5, numOut= self.nFeat, name = 'r6')
        #print("r6",r6.get_shape())
    
    nStack=self.nStack

    # Storage Table
    hg = [None] * nStack
    ll = [None] * nStack
    ll_ = [None] * nStack
    drop = [None] * nStack
    out = [None] * nStack
    out_ = [None] * nStack
    
    if self.tiny:
      sys.exit("right now you can exit~")
    else:
      with tf.variable_scope('stage_0'): #tf.name_scope('stage_0'):
        hg[0] = self._hourglass(train_stage, r6, self.nLow, self.nFeat*2, 'hourglass')
        #print("hg0",hg[0].get_shape())
        if self.TRAIN["use_dropout"]:
          drop[0] = tf.layers.dropout(hg[0], rate = self.dropout_rate, training = train_stage, name = 'dropout')
          ll[0] = self._conv_bn_relu(train_stage, drop[0], self.nFeat*2, 1,1, 'VALID', name = 'conv')
        else:
          ll[0] = self._conv_bn_relu(train_stage, hg[0], self.nFeat*2, 1,1, 'VALID', name = 'conv')
        ll_[0] =  self._conv_bn_relu(train_stage, ll[0], self.nFeat, 1, 1, 'VALID', 'll')
        out[0] = self._conv(train_stage, ll_[0], self.outDim, 1, 1, 'VALID', 'out', _use_bias=self.TRAIN["use_bias"])
        out_[0] = self._conv(train_stage, out[0], int(self.nFeat+self.nFeat/2), 1, 1, 'VALID', 'out_', _use_bias=self.TRAIN["use_bias"])
        #print("ll_0",ll_[0].get_shape())
        #print("pool1",pool1.get_shape())
        if self.df=="NHWC":
          cat1  = tf.concat([ll_[0], pool1], 3, name= "cat1")
        else:
          cat1  = tf.concat([ll_[0], pool1], 1, name= "cat1")
        cat1_ = self._conv(train_stage, cat1, int(self.nFeat+self.nFeat/2),1,1,'VALID','cat1_',_use_bias=self.TRAIN["use_bias"])
        int1  = tf.add_n([cat1_, out_[0]], name= 'int1')

      #TODO WHEN STACKS NOT 2, check APPENDIX A in the bottom

      with tf.variable_scope('stage_'+str(self.nStack-1)): #tf.name_scope('stage_' + str(self.nStack -1)):
        hg[self.nStack - 1] = self._hourglass(train_stage, int1, self.nLow, self.nFeat*2, 'hourglass')
        if self.TRAIN["use_dropout"]:
          drop[self.nStack-1] = tf.layers.dropout(hg[self.nStack-1], rate = self.dropout_rate, training = train_stage, name = 'dropout')
          ll[self.nStack - 1] = self._conv_bn_relu(train_stage, drop[self.nStack-1], self.nFeat*2, 1, 1, 'VALID', 'conv')
        else:
          ll[self.nStack - 1] = self._conv_bn_relu(train_stage, hg[self.nStack-1], self.nFeat*2, 1, 1, 'VALID', 'conv')
        
        ll_[self.nStack - 1] = self._conv_bn_relu(train_stage, ll[self.nStack - 1], self.nFeat*2, 1, 1, 'VALID', 'conv_')
        out[self.nStack - 1] = self._conv(train_stage, ll_[self.nStack - 1], self.outDim, 1,1, 'VALID', 'out', _use_bias=self.TRAIN["use_bias"])

    # transpose logits back to NHWC
    if self.df == "NCHW":
      for j in range(len(out)):
        out[j] = tf.transpose(out[j], [0, 2, 3, 1])
    
    logits = out[self.nStack - 1]
    n_logits=tf.concat([out[0],out[-1]], axis= 3 , name = 'final_output')	

    return logits, n_logits, n_img

  def _pad(self,inputs, pad_size=-1,name=None):
    if self.df=="NHWC":
      return tf.pad(inputs,[[0,0],[pad_size,pad_size],[pad_size,pad_size],[0,0]],name=name)
    else:
      return tf.pad(inputs,[[0,0],[0,0],[pad_size,pad_size],[pad_size,pad_size]],name=name)

  def _max_pool2d(self,inputs,pool_size=1, stride=1,padding="VALID",name="maxpool"):
    if self.df=="NCHW":
      order="channels_first"
    else:
      order="channels_last"
    return tf.layers.max_pooling2d(inputs, pool_size, stride,
           padding=padding,data_format=order,name=name)


  ## contrib.layers.conv2d.version
  def _contrib_conv(self,train,inputs,filters,kernel_size=1,strides=1,pad="VALID",name="conv",_use_bias=None):
    
    if self.df=="NCHW":
      input_dim=inputs.get_shape().as_list()[1] # NCHW, 1 for C!  
    else:
      input_dim=inputs.get_shape().as_list()[3] # NHWC, 3 for C!

    with tf.variable_scope(name):
      if not self.smart_conv or _use_bias==True:
        return tf.contrib.layers.conv2d(inputs,filters,kernel_size=kernel_size,stride=strides,
               weights_initializer=self._get_initializer(input_dim,kernel_size,kernel_size), #tf.contrib.layers.variance_scaling_initializer(),
               biases_initializer=self._get_initializer(input_dim,kernel_size,kernel_size), #tf.contrib.layers.variance_scaling_initializer(),
               trainable=train,padding=pad,data_format=self.df,scope="conv")
      else:
        return tf.contrib.layers.conv2d(inputs,filters,kernel_size=kernel_size,stride=strides,
               weights_initializer=self._get_initializer(input_dim,kernel_size,kernel_size), #tf.contrib.layers.variance_scaling_initializer(),
               biases_initializer=None, #tf.contrib.layers.variance_scaling_initializer(),
               trainable=train,padding=pad,data_format=self.df,scope="conv")


  ## layer.conv2d version
  def _layer_conv(self,train,inputs,filters,kernel_size=1,strides=1,pad="VALID",name="conv",_use_bias=None):
    if _use_bias is None:
      self.warning("remember to set bias flag for conv layer!")
      sys.exit()
    
    if self.df=="NCHW":
      input_dim=inputs.get_shape().as_list()[1] # NCHW, 1 for C!  
    else:
      input_dim=inputs.get_shape().as_list()[3] # NHWC, 3 for C!

    with tf.variable_scope(name):
      if self.df=="NCHW":
        order="channels_first"
      else:
        order="channels_last"

      kernel_initializer=self._get_initializer(input_dim, kernel_size, kernel_size)
      #kernel_initializer=tf.constant_initializer(0.0)

      bias_initializer = self._get_initializer(input_dim, kernel_size, kernel_size)
      #bias_initializer = tf.constant_initializer(0.0)
      if self.smart_conv and _use_bias==False:
        bias_initializer=None
      return tf.layers.conv2d(
           inputs, filters, kernel_size, 
           strides=strides,
           padding=pad,
           data_format=order,
           use_bias=_use_bias,  
           kernel_initializer=kernel_initializer,
           bias_initializer=bias_initializer,        
           trainable=train,
           name="conv")


  ## nn.conv2d version
  def _nn_conv(self, train, inputs, filters, kernel_size = 1, strides = 1, pad = 'VALID', name = 'conv',_use_bias=None):
    """ Spatial Convolution (CONV2D)
    Args:
      inputs			: Input Tensor (Data Type : NHWC)
      filters		: Number of filters (channels)
      kernel_size	: Size of kernel
      strides		: Stride
      pad				: Padding Type (VALID/SAME) # DO NOT USE 'SAME' NETWORK BUILT FOR VALID
      name			: Name of the block
    Returns:
      conv			: Output Tensor (Convolved Input)
    """
    with tf.variable_scope(name):
      if self.df=="NCHW":
        stride_list=[1,1,strides,strides]
        input_dim=inputs.get_shape().as_list()[1] # NCHW, 1 for C!
        
      else:
        stride_list=[1,strides,strides,1]
        input_dim=inputs.get_shape().as_list()[3] # NHWC, 3 for C!
      
      
      kernel = tf.get_variable('weights',
          initializer=self._get_initializer(),
          shape=([kernel_size, kernel_size, input_dim, filters]), 
          trainable=train)
      
      return tf.nn.conv2d(inputs, kernel, stride_list, padding=pad, data_format=self.df)
  

  def _conv(self, train, inputs, filters, kernel_size = 1, strides = 1, pad = 'VALID', name = 'conv', _use_bias=None):
    if self.TRAIN["conv_type"]=="nn_conv":
      return self._nn_conv(train,inputs,filters,kernel_size,strides,pad,name,_use_bias)
    elif self.TRAIN["conv_type"]=="layer_conv":
      return self._layer_conv(train,inputs,filters,kernel_size,strides,pad,name,_use_bias)
    elif self.TRAIN["conv_type"]=="contrib_conv":
      return self._contrib_conv(train,inputs,filters,kernel_size,strides,pad,name,_use_bias)
    else:
      self.warning("check your conv setting!")
      sys.exit()
    return 0


  def _get_initializer(self,IN=None,H=None,W=None,OUT=None):
    if self.TRAIN["initializer"]=="xavier":
      initializer = tf.contrib.layers.xavier_initializer(uniform=self.TRAIN["xavier_uniform"])
    elif self.TRAIN["initializer"]=="variance":
      initializer = tf.contrib.layers.variance_scaling_initializer(factor=self.TRAIN["var_factor"],mode=self.TRAIN["var_mode"],uniform=self.TRAIN["var_uniform"])
    elif self.TRAIN["initializer"]=="torch":
      stdv=1/np.sqrt(H*W*IN)
      initializer = tf.random_uniform_initializer(minval=-stdv,maxval=stdv)
    else:
      initializer = None
      self.warning("check your initializer!")
      sys.exit()
    return initializer
  

  def _conv_bn_relu(self, train, inputs, filters, kernel_size = 1, strides = 1, pad = 'VALID', name = 'conv_bn_relu', use_bias=True):
    """ Spatial Convolution (CONV2D) + BatchNormalization + ReLU Activation
    Args:
      inputs			: Input Tensor (Data Type : NHWC)
      filters		: Number of filters (channels)
      kernel_size	: Size of kernel
      strides		: Stride
      pad				: Padding Type (VALID/SAME) # DO NOT USE 'SAME' NETWORK BUILT FOR VALID
      name			: Name of the block
    Returns:
      norm			: Output Tensor
    """
    #TODO not sure about training, refer to https://stackoverflow.com/questions/50047653/set-training-false-of-tf-layers-batch-normalization-when-training-will-get-a
    with tf.variable_scope(name): #tf.name_scope(name):
      conv = self._conv(train, inputs, filters,\
                        kernel_size = kernel_size, strides = strides, pad = pad,\
                        name = 'conv', _use_bias=(not self.smart_conv))
      
      return self._bn_relu(train,conv)
  

  def _bn(self,train,inputs):
    return tf.layers.batch_normalization(
           inputs,momentum=0.1,epsilon=1e-5,center=True,scale=True,
           beta_initializer=tf.zeros_initializer(),
           gamma_initializer=tf.random_uniform_initializer(), #tf.ones_initializer(), #TODO This is important
           moving_mean_initializer=tf.zeros_initializer(),
           moving_variance_initializer=tf.ones_initializer(),
           training=train,
           trainable=train,
           fused=True
    )


  # contrib layers version
  def _contrib_bn_relu(self,train,inputs):
    return tf.contrib.layers.batch_norm(
      inputs,0.9,epsilon=1e-5,activation_fn=tf.nn.relu,is_training=train,scale=True,fused=True)
    
  # layers version
  def _layer_bn_relu(self, train, inputs):
    bn = tf.layers.batch_normalization(
      inputs, trainable=train, training=train, momentum=0.9,name="bn",fused=True)

    return tf.nn.relu(bn,name="relu")

  def _bn_relu(self,train,inputs):
    if self.TRAIN["use_bn"]:
      if self.TRAIN["first_bn_then_relu"]:
        return tf.nn.relu(self._bn(train,inputs))
      else:
        return self._bn(train,tf.nn.relu(inputs)) 
    else:
      return tf.nn.relu(inputs)

  def _conv_block(self, train, inputs, numOut, name = 'conv_block'):
    """ Convolutional Block
    Args:
      inputs	: Input Tensor
      numOut	: Desired output number of channel
      name	: Name of the block
    Returns:
      conv_3	: Output Tensor
    """
    if self.tiny:
      with tf.variable_scope(name): #tf.name_scope(name):
        norm=self._bn_relu(train,inputs)
        pad = self._pad(norm, 1, name= 'pad')
        conv = self._conv(train, pad, int(numOut), kernel_size=3, strides=1, pad = 'VALID', name= 'conv', _use_bias=self.TRAIN["use_bias"])
        return conv
    else:
      with tf.variable_scope(name): #tf.name_scope(name):
        with tf.variable_scope('norm_1'): #tf.name_scope('norm_1'):
          norm_1=self._bn_relu(train,inputs)
          conv_1 = self._conv(train, norm_1, int(numOut/2), kernel_size=1, strides=1, pad = 'VALID', name= 'conv', _use_bias=(not self.smart_conv))
        with tf.variable_scope('norm_2'): #tf.name_scope('norm_2'):
          norm_2=self._bn_relu(train,conv_1)
          pad = self._pad(norm_2, 1, name= 'pad')
          conv_2 = self._conv(train, pad, int(numOut/2), kernel_size=3, strides=1, pad = 'VALID', name= 'conv', _use_bias=(not self.smart_conv))
        with tf.variable_scope('norm_3'): #tf.name_scope('norm_3'):
          norm_3=self._bn_relu(train,conv_2)
          conv_3 = self._conv(train, norm_3, int(numOut), kernel_size=1, strides=1, pad = 'VALID', name= 'conv', _use_bias=self.TRAIN["use_bias"])
        return conv_3

  def _skip_layer(self, train, inputs, numOut, name = 'skip_layer'):
    """ Skip Layer
    Args:
      inputs	: Input Tensor
      numOut	: Desired output number of channel
      name	: Name of the bloc
    Returns:
      Tensor of shape (None, inputs.height, inputs.width, numOut)
    """
    with tf.variable_scope(name): #tf.name_scope(name):
      if self.df=="NCHW":
        numIn=inputs.get_shape().as_list()[1]
      else:
        numIn=inputs.get_shape().as_list()[3]
      
      if numIn==numOut:
        return inputs
      else:
        return self._conv(train, inputs, numOut, 
          kernel_size=1, strides = 1, name = 'conv',
          _use_bias=self.TRAIN["use_bias"])
        		

  def _residual(self, train, inputs, numOut, name = 'residual_block'):
    """ Residual Unit
    Args:
      inputs	: Input Tensor
      numOut	: Number of Output Features (channels)
      name	: Name of the block
    """
    with tf.variable_scope(name): #tf.name_scope(name):
      convb = self._conv_block(train, inputs, numOut)
      skipl = self._skip_layer(train, inputs, numOut)
      if self.modif:
        return tf.nn.relu(tf.add_n([convb, skipl], name = 'res_block'))
      else:
        return tf.add_n([convb, skipl], name = 'res_block')

  def _hourglass(self, train, inputs, n, numOut, name = 'hourglass'):
    """ Hourglass Module
    Args:
      inputs	: Input Tensor
      n		: Number of downsampling step
      numOut	: Number of Output Features (channels)
      name	: Name of the block
    """
    #TODO try to find a smart way to save conv.bias before batch_norm layer
    with tf.variable_scope(name): # tf.name_scope(name):
      # Upper branch
      up1 = self._residual(train, inputs, self.nFeat, name = 'up_1')
      up2 = self._residual(train, up1  , self.nFeat, name = 'up_2')
      up4 = self._residual(train, up2  , numOut    , name = 'up_4')
      
      # Lower branch
      pool = self._max_pool2d(inputs, [2,2], [2,2], padding='VALID')
      low1 = self._residual(train, pool, self.nFeat, name = 'low_1')
      low2 = self._residual(train, low1, self.nFeat, name = 'low_2')
      low5 = self._residual(train, low2, self.nFeat, name = 'low_5')
      
      if n > 1:
        low6=self._hourglass(train, low5, n-1, numOut, name = 'low_6')
      else:
        low6=self._residual(train, low5, numOut, name = 'low_6')
      low7=self._residual(train, low6, numOut, name = 'low_7')
      #print("low7",low7.get_shape())
      
      if self.df=="NCHW":
        low7 = tf.transpose(low7, [0, 2, 3, 1])
        #print("low7",low7.get_shape())
        #print("now low7", tf.shape(low7)[1:4])
        up5 = tf.image.resize_nearest_neighbor(low7, tf.shape(low7)[1:3]*2, name = 'upsampling')
        #print("up5",up5.get_shape())
        up5 = tf.transpose(up5, [0, 3, 1, 2])
        #print("up5",up5.get_shape())
      else:
        up5 = tf.image.resize_nearest_neighbor(low7, tf.shape(low7)[1:3]*2, name = 'upsampling')
      #print("up4",up4.get_shape())
      #print("up5",up5.get_shape())
      return tf.add_n([up4,up5], name='out_hg')
  
  def _cal_loss(self, n_logits, lbls_pl):
    """ # another way (previously used by wbenbhi)
    loss = tf.reduce_mean(
      tf.nn.sigmoid_cross_entropy_with_logits(
        logits = n_logits,  # logits_train,
        labels = tf.stack([lbls_pl for ii in range(self.nStack)],axis=1 , name = 'the_gts')   # lbls_pl
        ), name= 'cross_entropy_loss')
    
    """
    loss = tf.losses.mean_squared_error(lbls_pl,n_logits[:,:,:,:self.outDim]) \
          + tf.losses.mean_squared_error(lbls_pl,n_logits[:,:,:,self.outDim:])
    return loss

  def _cal_grads(self, loss):
    if self.TRAIN["grads"] == "speed":
      # calculate tower grads by using OpenAI's implementation
      # of checkpointed gradients (better for memory)
      grads = msg.gradients_speed(
          loss, tf.trainable_variables(), gate_gradients=True)
    elif self.TRAIN["grads"] == "mem":
      # calculate tower grads by using OpenAI's implementation
      # of checkpointed gradients (better for speed)
      grads = msg.gradients_memory(
          loss, tf.trainable_variables(), gate_gradients=True)
    elif self.TRAIN["grads"] == "tf":
      # calculate tower grads by using TF implementation
      print("Using tensorflow gradients")
      #grads = tf.gradients(
      #    loss, tf.trainable_variables())
      grads_var = self.optimizer.compute_gradients(loss, tf.trainable_variables())
      grads = [x[0] for x in grads_var]
      #print(grads)
    else:
      print("Gradient option not supported. Check config")
      grads = None

    return grads

  def _cal_keypoints(self,pred2d,label2d):
    l_idx=np.argmax(label2d)
    l_u=l_idx//64
    l_v=l_idx%64
    if self.almost_zero(label2d[l_u,l_v]):
      return 0,0
    else:
      p_idx=np.argmax(pred2d)
      p_u=p_idx//64
      p_v=p_idx%64
      if ((p_u-l_u)**2+(p_v-l_v)**2)**0.5<64/10*self.TRAIN["acc_threshold"]:
        return 1,1
      else:
        return 0,1

  """
          train_acc_cands,train_acc_valids,train_acc_val,train_acc2=self._cal_acc_metrics(
          train_pred_stack, feed_dict)
  """
  def _cal_acc_metrics(self, pred_stack, feed_dict):
    
    acc_cands=np.zeros((1,self.num_classes))
    acc_valids=np.zeros((1,self.num_classes))
    #print("stack",np.shape(pred_stack))
    for gpu_i in range(self.n_gpus_avail):
      batch_size=np.shape(pred_stack[gpu_i])[0]
      for batch_i in range(batch_size):
        for kp_i in range(self.num_classes):
          tmp_cands,tmp_valids=self._cal_keypoints(
            pred_stack[gpu_i][batch_i,:,:,102+kp_i], 
            feed_dict[self.lbls_pl_list[gpu_i]][batch_i][:,:,kp_i])
          acc_cands[0,kp_i]  += tmp_cands
          acc_valids[0,kp_i] += tmp_valids

    sum_acc_cands=np.sum(acc_cands)
    sum_acc_valids=np.sum(acc_valids)

    if self.almost_zero(sum_acc_valids):
      acc_val=-1
    else:
      acc_val=sum_acc_cands/sum_acc_valids

    acc2=None

    return acc_cands, acc_valids, acc_val, acc2


  def _accuracy_computation(self, logits, gtMaps):
    """ Computes accuracy tensor
    shape N * H * W * C
    """
    joints_accur1 = []
    joints_accur2 = []
    for i in range(self.num_classes):
      joints_accur1.append(self._accur(logits[:,:,:,i],gtMaps[:,:,:,i],int(logits.get_shape()[0]),False))
      joints_accur2.append(self._accur(logits[:,:,:,i],gtMaps[:,:,:,i],int(logits.get_shape()[0]),True))
    return joints_accur1,joints_accur2

  def _accur(self, pred, gtMap, num_image, legacy):
    """ Given a Prediction batch (pred) and a Ground Truth batch (gtMaps),
    returns one minus the mean distance.
    Args:
      pred		: Prediction Batch (shape = num_image x 64 x 64)
      gtMaps		: Ground Truth Batch (shape = num_image x 64 x 64)
      num_image 	: (int) Number of images in batch
    Returns:
      (float)
    """
    if legacy:
      """ legacy metrics pure normalized distance
      """
      err = tf.to_float(0)
      cnt = tf.to_float(0)
      for i in range(num_image):
        #err = tf.Print(err, [err],"ERR"+str(i)+" is ")
        val = self._check_valid(gtMap[i])
        err = tf.add(err, val*self._compute_err(pred[i], gtMap[i]))
        cnt = tf.add(cnt, val)
      #cnt = tf.cond(tf.less(cnt, 1e-7), lambda: err, lambda: cnt)
      return tf.cond(tf.less(cnt,1e-7), lambda: 0.0, lambda: 1.0-err/cnt)#tf.subtract(tf.to_float(1), err/cnt)

    else:
      """ 6DoF metrics: percentage of points that dist_err < outRes/10
      """
      # @cand ~ dist <thres
      # @others ~ dist >thres
      # @bads ~ label==-1
      # total=@cand+@bad+@others
      # percentage=cand/(cand+others)
      candCount=tf.to_float(0)
      validCount=0

      for i in range(num_image):
        is_cand, is_valid=self._judge_rel_dist(pred[i],gtMap[i],64/10)
        candCount = candCount + is_cand*is_valid
        validCount = validCount + is_valid
      return tf.cond(tf.less(validCount, 1e-7), lambda: -1.0, lambda: candCount/validCount)
      

  def _judge_rel_dist(self,u,v,length):
    u_x,u_y = self._argmax(u)
    v_x,v_y = self._argmax(v)
    dist = tf.sqrt(tf.square(tf.to_float(u_x - v_x)) + tf.square(tf.to_float(u_y - v_y)))/length
    is_cand = tf.to_float(tf.less_equal(dist,self.acc_threshold))
    is_valid = tf.to_float(tf.greater_equal(v[v_x,v_y],1e-7))
    return is_cand, is_valid
  
  def _compute_err(self, u, v):
    """ Given 2 tensors compute the euclidean distance (L2) between maxima locations
    Args:
      u		: 2D - Tensor (Height x Width : 64x64 )
      v		: 2D - Tensor (Height x Width : 64x64 )
    Returns:
      (float) : Distance (in [0,1])
    """
    u_x,u_y = self._argmax(u)
    v_x,v_y = self._argmax(v)
    #u_y=tf.Print(u_y,[u_y],"u_y=") if want to check
    result=tf.divide(tf.sqrt(tf.square(tf.to_float(u_x - v_x)) + tf.square(tf.to_float(u_y - v_y))), tf.to_float(91))
    #result=tf.Print(result,[u_x,u_y,v_x,v_y,result],"uxuyvxvy res: ") another example
    return result
  
  def _check_valid(self, gtMap):
    x,y = self._argmax(gtMap)
    # only invalid when x,y all zero and the value corr. there is also zero (means -1,-1 in label)
    vgeq0=tf.to_float(tf.greater_equal(gtMap[x,y],1e-7))
    return vgeq0 

  def train(self, path=None):
    ''' Main function to train a network from scratch or from checkpoint
    '''

    # get dataset reader
    print("Fetching dataset")
    self.parser = imp.load_source("parser",
                                  os.getcwd() + '/dataset/' +
                                  self.DATA["name"] + '.py')

    # report batch size and gpus to use
    self.batch_size = int(self.TRAIN["batch_size"])
    self.n_gpus = int(self.TRAIN['gpus'])
    print("Training with %d GPU's" % self.n_gpus)
    print("Training with batch size %d" % self.batch_size)

    # gpus available
    self.n_gpus_avail = self.gpu_available()
    print("Number of GPU's available is %d" % self.n_gpus_avail)
    assert(self.n_gpus == self.n_gpus_avail)

    # calculate batch size per gpu
    self.batch_size_gpu = int(self.batch_size / self.n_gpus_avail)
    assert(self.batch_size % self.n_gpus == 0)
    assert(self.batch_size_gpu > 0)
    print("This means %d images per GPU" % self.batch_size_gpu)

    # import dataset
    self.dataset = self.parser.read_data_sets(self.DATA)
    
    self.bestValidAtEpoch=0
    
    # get learn rate from config file
    with tf.Graph().as_default(), tf.device('/cpu:0'):
      self.lrate = self.TRAIN['lr']
      with tf.variable_scope("learn_rate"):
        lr_init = tf.constant(self.lrate)
        self.learn_rate_var = tf.get_variable(name="learn_rate",
                                              initializer=lr_init,
                                              trainable=False)
        # report the current learn rate to tf log
        tf.summary.scalar('learn_rate', self.learn_rate_var)

      with tf.variable_scope("trainstep"):
        self.optimizer = MyRMSProp(learning_rate= self.learn_rate_var,decay=0.99,  momentum=0.0, epsilon=1e-16,centered=False) ##TODO tf.train.RMSPropOptimizer
        print(self.optimizer.get_slot_names())

        #self.optimizer = tf.train.GradientDescentOptimizer(learning_rate=self.learn_rate_var)
        
        #self.optimizer = tf.train.MomentumOptimizer(learning_rate=self.learn_rate_var, momentum=0.9)        

        #FIXME This is the ADAM used by bonnet
        #self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learn_rate_var,
        #                                        beta1=0,#self.TRAIN["decay1"],
        #                                        beta2=0)#self.TRAIN["decay2"],
        #                                        #epsilon=self.TRAIN["epsilon"])

      # inititialize inference graph
      #self.obs_1output_list=[]
      #self.obs_2conv_list=[]
      #self.obs_4relu_list=[]
      #self.obs_6conv_list=[]
      #self.obs_8relu_list=[]
      #self.obs_8output_list=[]

      self.n_logits_train_list = []
      self.n_logits_valid_list = []
      self.train_losses = []
      self.valid_losses = []
      self.tower_grads = []
      self.img_pl_list = []
      self.lbls_pl_list = []

      print("Initializing network")
      with tf.name_scope("train_model"):
        with tf.variable_scope("model"):
          for i in range(self.n_gpus):
            with tf.device(self.assign_to_device('/gpu:%d' % i)):
              print(' TRAINING GRAPH '.center(80, '*'))
              print(' GRAPH GPU:%d ' % i)

              #Create placeholders for images input and labels
              img_pl, lbls_pl = self.placeholders(
                [self.DATA["img_prop"]["depth"], self.DATA["img_prop"]["hm_depth"]] , self.batch_size_gpu)
              self.img_pl_list.append(img_pl)
              self.lbls_pl_list.append(lbls_pl)

              # CORE GRAPH(Training phase)
              _, n_logits_train, _ = self.build_graph(img_pl, True)  # train
              self.n_logits_train_list.append(n_logits_train)

              # LOSS and GRADIENTS
              train_loss = self._cal_loss(n_logits_train, lbls_pl)
              grads=self._cal_grads(train_loss) #self.optimizer.compute_gradients(train_loss)

              # Join together for input of optimizer.apply_gradients() function
              grads_and_vars = list(zip(grads, tf.trainable_variables()))

              # Append to the list of gradients and losses
              self.train_losses.append(train_loss)
              self.tower_grads.append(grads_and_vars)

              # Reuse variables for the next tower.
              tf.get_variable_scope().reuse_variables()
              print('*' * 80)

      with tf.name_scope("test_model"):
        with tf.variable_scope("model", reuse=True):
          for i in range(self.n_gpus):
            with tf.device('/gpu:%d' % i):
              print(' TESTING GRAPH '.center(80, '*'))
              print(' GRAPH GPU:%d ' % i)

              # Fetch placeholders
              img_pl = self.img_pl_list[i]
              lbls_pl = self.lbls_pl_list[i]

              # CORE GRAPH(Testing phase)
              _, n_logits_valid, _ = self.build_graph(img_pl, False)  # test
              self.n_logits_valid_list.append(n_logits_valid)
              
              # Only calculating loss for statistics
              valid_loss = self._cal_loss(n_logits_valid, lbls_pl)
              self.valid_losses.append(valid_loss)
              print('*' * 80)

      # print number of parameters
      n_parameters = 0
      for var in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='model'):
      #for var in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='model'):
        var_params = np.prod(var.get_shape().as_list())
        n_parameters += var_params
        if "conv_256_to_128/batch_normalization/gamma" in var.name:
          self.bn_gamma=var
       #print(var.name, var_params)

      #for var in tf.get_collection(tf.GraphKeys.UPDATE_OPS):
      #  var_params=np.prod(var.get_shape().as_list())
      #  n_parameters += var_params
      #  print(var.name, var_params)
      for v in tf.trainable_variables():
        print(v)


      print("Total number of parameters in network: ", n_parameters)

      def get_grad(grad,var,description,relation="include"):
        if relation in ["include","equal"]:
          if grad is not None:
            #print("TRY TO FIND",description,"IN",var.name)
            if (relation=="include" and description in var.name) \
                or (relation=="equal" and description == var.name):
              #print(var.name, grad)
              return grad
            else:
              #print("Cannot find any grads with relationship '"+relation+"' for "+description )
              return None
          else:
            #print("Gradient is None, check this out")
            return None
        else:
          self.warninig("I don't know the relation, check collect_grad usage!")
          sys.exit()
          return None

      with tf.variable_scope("optimizer", reuse=tf.AUTO_REUSE):
        # We must calculate the mean of each gradient. Note that this is the
        # synchronization point across all towers.
        if self.TRAIN["multi_gpu"]:
          self.grads = self.average_gradients(self.tower_grads)
          self.n_train_loss = tf.add_n(self.train_losses)
          self.n_valid_loss = tf.add_n(self.valid_losses)
        else:
          self.grads =self.tower_grads[0]
          self.n_train_loss = self.train_losses[0]
          self.n_valid_loss = self.valid_losses[0]
        
        
        #self.n_obs_1output_stack=tf.stack(self.obs_1output_list)
        #self.n_obs_4relu_stack=tf.stack(self.obs_4relu_list)
        #self.n_obs_8output_stack=tf.stack(self.obs_8output_list)
        

        # Hefre we use n_logits_train/valid_stack to calculate acc later
        # The dimension is n_gpu *n_batch * H * W * (2*n_channel)
        # In our case is 1 * 4 * 64 * 64 * 204
        # For the last dim, the mid_output is 0:102, and the final_output is 102:204 
        self.n_logits_train_stack = tf.stack(self.n_logits_train_list)
        self.n_logits_valid_stack = tf.stack(self.n_logits_valid_list)
        
        #TODO modified for debug use
        #if self.TRAIN['summary']:
          
          #TODO for linear model
          #for var in tf.trainable_variables():
            #if "kernel" in var.name:
              #print("found",var.name)
              #self.obs_lin_conv_kernel=var
              #printF("t"+str(self.step)+"_tf_lin_conv_weight.txt",self.sess.run(var))
            #if "bias" in var.name:
              #print("found",var.name)
              #self.obs_lin_conv_bias=var
              #printF("t"+str(self.step)+"_tf_lin_conv_bias.txt",self.sess.run(var))


          #for grad, var in self.grads:
            #FIXME wrong logic here! 
            #self.obs_grad_r6_skip_bias=get_grad(grad,var,"r6/skip_layer/conv/conv/bias:0")
            #self.obs_grad_r6_skip_kernel=get_grad(grad,var,"r6/skip_layer/conv/conv/kernel:0")
            #self.obs_grad_r6_cb_conv_bias=get_grad(grad,var,"r6/conv_block/norm_3/conv/conv/bias")
            #self.obs_grad_r6_cb_conv_kernel=get_grad(grad,var,"r6/conv_block/norm_3/conv/conv/kernel")
            #self.obs_grad_r6_cb_bn_gamma=get_grad(grad,var,"r6/conv_block/norm_3/batch_normalization/gamma")
            #self.obs_grad_r6_cb_bn_beta=get_grad(grad,var,"r6/conv_block/norm_3/batch_normalization/beta")
            #self.obs_grad_r5_cb_conv_bias=get_grad(grad,var,"r5/conv_block/norm_3/conv/conv/bias")
            #self.obs_grad_r5_cb_conv_kernel=get_grad(grad,var,"r5/conv_block/norm_3/conv/conv/kernel")
            #self.obs_grad_r5_cb_bn_gamma=get_grad(grad,var,"r5/conv_block/norm_3/batch_normalization/gamma")
            #self.obs_grad_r5_cb_bn_beta=get_grad(grad,var,"r5/conv_block/norm_3/batch_normalization/beta")
            #self.obs_grad_r1_conv_kernel=get_grad(grad,var,"conv_256_to_128/conv/conv/kernel")
            #self.obs_grad_r1_conv_bias=get_grad(grad,var,"conv_256_to_128/conv/conv/bias")

            #self.obs_grad_r1_bn_gamma=get_grad(grad,var,"conv_256_to_128/batch_normalization/gamma")
            #self.obs_grad_r1_bn_beta=get_grad(grad,var,"conv_256_to_128/batch_normalization/beta")

            #TODO for linear model
            #tmp_val=get_grad(grad,var,"kernel")
            #if tmp_val is not None:
            #  self.obs_lin_grad_conv_kernel=tmp_val
            #tmp_val=get_grad(grad,var,"bias")
            #if tmp_val is not None:
            #  self.obs_lin_grad_conv_bias=tmp_val


        #TODO summary for PARAMETERS and GRADIENTS
        #tf.summary.histogram(var.op.name + '/gradients', grad) 
        Model_variables = tf.GraphKeys.MODEL_VARIABLES
        Global_Variables = tf.GraphKeys.GLOBAL_VARIABLES
        #####
        all_vars = tf.get_collection(Model_variables)
        tf.summary.histogram("/weights",all_vars)

        # Apply the gradients to adjust the shared variables.
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
          self.train_op = self.optimizer.apply_gradients(self.grads)

      # define the best performance so far in the validation set
      self.best_acc_validation = 0

      # periodically report accuracy 
      print("Reporting accuracy every ", self.TRAIN["acc_report_epochs"], " epochs")
      
      #FIXME not sure this summary doing, just comment them
      # with tf.variable_scope("accuracies"):
      #   self.train_accuracy = tf.Variable(0.0, name="train", trainable=False)
      #   self.validation_accuracy = tf.Variable(
      #       0.0, name="validation", trainable=False)
      #   #self.test_accuracy = tf.Variable(0.0, name="test", trainable=False)
      #   # summaries for the accuracies (to be evaluated later on)
      #   tf.summary.scalar('train_accuracy', self.train_accuracy)
      #   tf.summary.scalar('validation_accuracy', self.validation_accuracy)
      #   #tf.summary.scalar('test_accuracy', self.test_accuracy)

      # with tf.variable_scope("loss_value"):
      #   # Add a scalar summary for the snapshot loss.
      #   self.train_loss = tf.Variable(0.0, name="train_loss", trainable=False)
      #   tf.summary.scalar('train', self.train_loss)

      #   self.train_batch_acc = tf.Variable(
      #       0.0, name="train_batch_acc", trainable=False)
      #   tf.summary.scalar('batch_acc', self.train_batch_acc)

      # Build the summary Tensor based on the TF collection of Summaries.
      self.summary = tf.summary.merge_all()

      # Add the variable initializer Op.
      self.init = tf.global_variables_initializer()


##TODO for seeing params in rmsprop
#      for var in tf.trainable_variables():
#        rms=self.optimizer.get_slot(var, "rms")
#        print(rms)



      # Create a saver for writing training checkpoints.
      self.saver = tf.train.Saver(save_relative_paths=True)

      # xla stuff for faster inference (and soft placement for low ram device)
      gpu_options = tf.GPUOptions(allow_growth=True, force_gpu_compatible=True)
      config = tf.ConfigProto(allow_soft_placement=True,
                              log_device_placement=False, gpu_options=gpu_options)
      config.graph_options.optimizer_options.global_jit_level = tf.OptimizerOptions.OFF

      # start a session
      self.sess = tf.Session(config=config)

      # Instantiate a SummaryWriter to output summaries and the Graph.
      self.log_dir = self.log + '/lr_' + str(self.lrate)
      print("Saving this iteration of training in %s" % self.log_dir)
      self.summary_writer = tf.summary.FileWriter(
          self.log_dir, self.sess.graph)

      # Run the Op to initialize the variables.
      self.sess.run(self.init)

      # if path to model is give, try to restore:
      if path is not None:
        self.restore_session(path)


      # print weight
      tmp_path=self.log + '/trainings/'
      if not tf.gfile.Exists(tmp_path):
        tf.gfile.MakeDirs(tmp_path)

      def printF(path,array):
        f=open(tmp_path+path,"w")
        f.write(" ".join([str(x) for x in np.reshape(array,(1,-1))[0]]))
        f.close()

      # do the training
      print("Training model")

      # Start the training loop
      # Here orignal bonnet use dataset/batch_size, 
      # but we are using fixed value (like 1000 steps per epoch) 
      # to acheive kind of random sampling from training set
      steps_per_epoch = self.TRAIN["steps_per_epoch"]
      
      acc_report_steps = int(self.TRAIN["acc_report_epochs"] * steps_per_epoch)
      max_steps = int(self.TRAIN["max_epochs"] * steps_per_epoch)
      print("Training network %d epochs (%d iterations at batch size %d)" %
            (self.TRAIN["max_epochs"], max_steps, self.TRAIN["batch_size"]))

      # calculate the decay steps with the batch size and num examples
      self.decay_steps = int(self.TRAIN["lr_decay"] * steps_per_epoch)
      self.decay_rate = float(self.TRAIN["lr_rate"])
      print("Decaying learn rate by %f every %d epochs (%d steps)" %
            (self.decay_rate, self.TRAIN["lr_decay"], self.decay_steps))
      

      #observation variables, lists and arrays

      # for loss
      self.epoch_train_loss=None
      self.epoch_valid_loss=None
      self.epoch_train_loss_list=None
      self.epoch_valid_loss_list=None

      # for accuracy
      self.epoch_train_acc_cands=None # 1*channel array 
      self.epoch_train_acc_valids=None # 1*channel array
      self.epoch_train_acc_list=None # step list for mean_acc
      
      self.epoch_valid_acc_cands=None # 1*channel array
      self.epoch_valid_acc_valids=None # 1*channel array
      self.epoch_valid_acc_list=None # step list for mean_acc

      self.epoch_train_acc2_joints=None
      self.epoch_train_acc2_list=None
      self.epoch_valid_acc2_joints=None
      self.epoch_valid_acc2_list=None

      # global watching list
      self.global_train_loss_list=[] # receive for each step
      self.global_valid_loss_list=[] # receive for each step
      self.global_train_acc_list=[]  # receive for each epoch
      self.global_train_acc2_list=[] # receive for each epoch
      self.global_valid_acc_list=[]  # receive for each epoch
      self.global_valid_acc2_list=[] # receive for each epoch

      for self.step in range(max_steps):

        # clear the observations
        if self.step % steps_per_epoch==0:
          self.epoch_train_loss=0
          self.epoch_valid_loss=0
          self.epoch_train_loss_list=[]
          self.epoch_valid_loss_list=[]
          
          self.epoch_train_acc_cands=np.zeros((1,self.num_classes))
          self.epoch_train_acc_valids=np.zeros((1,self.num_classes))
          
          self.epoch_valid_acc_cands=np.zeros((1,self.num_classes))
          self.epoch_valid_acc_valids=np.zeros((1,self.num_classes))

          self.epoch_train_acc2_joints=np.zeros((1,self.num_classes))
          self.epoch_valid_acc2_joints=np.zeros((1,self.num_classes))
          
          self.epoch_train_accs=[]
          self.epoch_valid_accs=[]

        # learn rate decay
        if self.step % self.decay_steps == 0 and self.step > 0:
          assign_lr = self.learn_rate_var.assign(
              self.sess.run(self.learn_rate_var)/self.decay_rate)#,options=options, run_metadata=run_metadata) / self.decay_rate)
          self.sess.run(assign_lr)#,options=options, run_metadata=run_metadata)  # assign the value to the node
          print("Decreased learning rate to: ",
                self.sess.run(self.learn_rate_var))#,options=options, run_metadata=run_metadata))
        
        # fill in the dictionaries
        start_time = time.time()
        if self.stickness:
          if self.first_stick:
            self.first_stick=False
            feed_dict, namelist = self.fill_feed_dict(self.dataset.train,
                                            self.img_pl_list,
                                            self.lbls_pl_list,
                                            self.batch_size_gpu)

        else:
          feed_dict, namelist = self.fill_feed_dict(self.dataset.train,
                                            self.img_pl_list,
                                            self.lbls_pl_list,
                                            self.batch_size_gpu)
          if self.print_name:
            for item in namelist:
              print(item)
        #img=feed_dict[self.img_pl_list[0]]
        #cv2.namedWindow("Image")
        #cv2.imshow("Image", img[0])
        #cv2.waitKey (0)
        #self.plt_save(img[0],self.log+"trainings/00000.png")
        
        if self.TRAIN["save_image"]:
          tmp_path=self.log + '/trainings/'
          if not tf.gfile.Exists(tmp_path):
            tf.gfile.MakeDirs(tmp_path)          

          for gpu_i in range(self.n_gpus):
            the_imgs=feed_dict[self.img_pl_list[gpu_i]]
            the_lbls=feed_dict[self.lbls_pl_list[gpu_i]]
            for img_i,the_img in enumerate(the_imgs):
              self.plt_save(the_img,"%strainings/s%d_gpu_%d_idx%d_img.png"%(
                                   self.log, self.step, gpu_i, img_i))

            img_val,lbls_val= self.sess.run(
              [self.img_pl_list[gpu_i],self.lbls_pl_list[gpu_i]],
              feed_dict=feed_dict)
            
            self.plt_save(
              np.concatenate((img_val[0],img_val[1],img_val[2],img_val[3]),axis=1),\
              self.log + 'trainings/s'+str(self.step)+"_gpu_"+str(gpu_i)+"_img.png"
            )

            self.plt_save(
              np.sum(np.concatenate((lbls_val[0],lbls_val[1],lbls_val[2],lbls_val[3]),axis=1),axis=2),\
              self.log+'trainings/s'+str(self.step)+"_gpu_hms.png"
            )

        duration_get_batch = time.time() - start_time
        # Run one step of the model in all gpus
        train_start_time = time.time()
        # This is with both forward and backward
        '''
        for var in tf.trainable_variables():
          if "conv3_256/conv/conv/kernel:0" in var.name:
            print("found",var.name)
            printF("t"+str(self.step)+"_tf_2conv_weight.txt",self.sess.run(var,feed_dict=feed_dict))
          if "conv3_256/conv/conv/bias:0" in var.name:
            print("found",var.name)
            printF("t"+str(self.step)+"_tf_2conv_bias.txt",self.sess.run(var,feed_dict=feed_dict))
        '''

        #TODO Trainnign phase has changed by adding observations
        """ observing session mode
        _, train_loss_val, train_pred_stack, obs_1output_stack, obs_4relu_stack, obs_8output_stack =       \
          self.sess.run(
            [self.train_op, self.n_train_loss, self.n_logits_train_stack, self.n_obs_1output_stack, self.n_obs_4relu_stack, self.n_obs_8output_stack ],\
            feed_dict=feed_dict
          )
        """

        #if self.step==0:
        #  bn_gamma=self.sess.run(self.bn_gamma)
        #  printF("t0_bn1_gamma.txt",bn_gamma)
        ##  print("pause here")
        #  xx=input()

        if self.TRAIN["print_tensor"]:
          train_loss_val,train_pred_stack, \
          lin_0input, lin_output, \
          lin_conv_kernel, lin_conv_bias \
          =self.sess.run(\
           [self.n_train_loss,self.n_logits_train_stack, \
            self.obs_lin_0input, self.obs_lin_output, \
            self.obs_lin_conv_kernel, self.obs_lin_conv_bias
           ], feed_dict=feed_dict)
          #print("step",self.step)
          #print(lin_conv_bias)

          _,train_loss_val,train_pred_stack, \
          lin_0input, lin_output, \
          lin_conv_kernel, lin_conv_bias,\
          lin_grad_conv_kernel,lin_grad_conv_bias\
          =self.sess.run(\
           [self.train_op,self.n_train_loss,self.n_logits_train_stack, \
            self.obs_lin_0input, self.obs_lin_output, \
            self.obs_lin_conv_kernel, self.obs_lin_conv_bias, \
            self.obs_lin_grad_conv_kernel, self.obs_lin_grad_conv_bias \
           ], feed_dict=feed_dict)
          

          #print("step",self.step)
          #print(lin_grad_conv_bias)
          #cc=input()
          """
          _,train_loss_val,train_pred_stack, \
        grad_r6_skip_bias,grad_r6_skip_kernel, \
        grad_r6_cb_conv_bias,grad_r6_cb_conv_kernel, \
        grad_r6_cb_bn_gamma,grad_r6_cb_bn_beta, \
        grad_r5_cb_conv_bias,grad_r5_cb_conv_kernel, \
        grad_r5_cb_bn_gamma,grad_r5_cb_bn_beta, \
        grad_r1_conv_kernel,grad_r1_conv_bias, \
        grad_r1_bn_gamma, grad_r1_bn_beta \
        =self.sess.run( \
        [self.train_op,self.n_train_loss,self.n_logits_train_stack, \
         self.obs_grad_r6_skip_bias,self.obs_grad_r6_skip_kernel, \
         self.obs_grad_r6_cb_conv_bias, self.obs_grad_r6_cb_conv_kernel, \
         self.obs_grad_r6_cb_bn_gamma,self.obs_grad_r6_cb_bn_beta, \
         self.obs_grad_r5_cb_conv_bias, self.obs_grad_r5_cb_conv_kernel, \
         self.obs_grad_r5_cb_bn_gamma,self.obs_grad_r5_cb_bn_beta, \
         self.obs_grad_r1_conv_kernel, self.obs_grad_r1_conv_bias, \
         self.obs_grad_r1_bn_gamma,self.obs_grad_r1_bn_beta
         ], feed_dict=feed_dict)
          """
          #TODO for first touch of the gradient backprop (in the higher layers)
          """        
          printF("t"+str(self.step)+"_tf_grad_r6_skip_bias.txt",grad_r6_skip_bias)
          printF("t"+str(self.step)+"_tf_grad_r6_skip_kernel.txt",grad_r6_skip_kernel)
          printF("t"+str(self.step)+"_tf_grad_r6_cb_conv_bias.txt",grad_r6_cb_conv_bias)
          printF("t"+str(self.step)+"_tf_grad_r6_cb_conv_kernel.txt",grad_r6_cb_conv_kernel)
          printF("t"+str(self.step)+"_tf_grad_r6_cb_bn_gamma.txt",grad_r6_cb_bn_gamma)
          printF("t"+str(self.step)+"_tf_grad_r6_cb_bn_beta.txt",grad_r6_cb_bn_beta)
          printF("t"+str(self.step)+"_tf_grad_r5_cb_conv_bias.txt",grad_r5_cb_conv_bias)
          printF("t"+str(self.step)+"_tf_grad_r5_cb_conv_kernel.txt",grad_r5_cb_conv_kernel)
          printF("t"+str(self.step)+"_tf_grad_r5_cb_bn_gamma.txt",grad_r5_cb_bn_gamma)
          printF("t"+str(self.step)+"_tf_grad_r5_cb_bn_beta.txt",grad_r5_cb_bn_beta)
          """
          #TODO for last touch of the grad backprop (in the front layers)
          #printF("t"+str(self.step)+"_tf_grad_r1_conv_bias.txt",grad_r1_conv_bias)
          #printF("t"+str(self.step)+"_tf_grad_r1_conv_kernel.txt",grad_r1_conv_kernel)
          #printF("t"+str(self.step)+"_tf_grad_r1_bn_gamma.txt",grad_r1_bn_gamma)
          #printF("t"+str(self.step)+"_tf_grad_r1_bn_beta.txt",grad_r1_bn_beta)
          

          #TODO printF for linear model 
          """ 
          printF("t"+str(self.step)+"_tf_lin_0input.txt",lin_0input)
          printF("t"+str(self.step)+"_tf_lin_output.txt",lin_output)
          printF("t"+str(self.step)+"_tf_lin_conv_kernel.txt",lin_conv_kernel)
          printF("t"+str(self.step)+"_tf_lin_conv_bias.txt",lin_conv_bias)
          printF("t"+str(self.step)+"_tf_lin_grad_conv_kernel.txt",lin_grad_conv_kernel)
          printF("t"+str(self.step)+"_tf_lin_grad_conv_bias.txt",lin_grad_conv_bias)
          """

        else:
          _,train_loss_val,train_pred_stack=self.sess.run(
            [self.train_op,self.n_train_loss,self.n_logits_train_stack],
            feed_dict=feed_dict)
     

        """
        _,train_loss_val,train_pred_stack=self.sess.run( \
          [self.train_op,self.n_train_loss,self.n_logits_train_stack], \
          feed_dict=feed_dict)"""

        #TODO working for observation session mode
        #printF("t"+str(self.step)+"_tf_1output.txt",obs_1output_stack)
        #printF("t"+str(self.step)+"_tf_4relu_output.txt",obs_4relu_stack)
        #printF("t"+str(self.step)+"_tf_8relu_output.txt",train_pred_stack)
        #printF("t"+str(self.step)+"_tf_8output.txt",obs_8output_stack)
        
        duration_train_step = time.time() - train_start_time

        if self.step % self.TRAIN["summary_freq"] == 0:
          print(">>> Now exam time for training")

          n_train_loss_value=train_loss_val # scalar
          img_value=feed_dict[self.img_pl_list[0]][0] # 256*256*3
          lbls_value=feed_dict[self.lbls_pl_list[0]][0] # 64*64*102
          n_logits_value=train_pred_stack[0] # 4*64*64*204
          
          #print("shape:",np.shape(img_value),np.shape(lbls_value),np.shape(lbls_value),np.shape(n_logits_value))
          tmp_path=self.log + '/trainings/'
          if not tf.gfile.Exists(tmp_path):
            tf.gfile.MakeDirs(tmp_path)
          tmp_header="t"+str(self.step)

          # 1. draw raw_img
          #print(type(img_value),np.shape(img_value))
          plt.imshow(self.normalize(img_value[:,:,:]))
          plt.savefig(tmp_path+tmp_header+"_1img.png")
          plt.close()

          # 2. draw lbl/int/pred heatmaps
          # sub each, and one total (3+1)
          huge_lbl=np.zeros((11*64,10*64))
          huge_int=np.zeros((11*64,10*64))
          huge_pred=np.zeros((11*64,10*64))
          mask_lbl=np.zeros((64,64))
          mask_int=np.zeros((64,64))
          mask_pred=np.zeros((64,64))
          for i in range(11):
            for j in range(10):
              channel=i*10+j
              if channel>=102:
                break
              huge_lbl[i*64:i*64+64,j*64:j*64+64]=lbls_value[:,:,channel]
              huge_int[i*64:i*64+64,j*64:j*64+64]=n_logits_value[0,:,:,channel]
              huge_pred[i*64:i*64+64,j*64:j*64+64]=n_logits_value[0,:,:,102+channel]
              
              lbl_idx=np.argmax(lbls_value[:,:,channel])
              lbl_u=lbl_idx//64
              lbl_v=lbl_idx%64
              if lbls_value[lbl_u,lbl_v,channel]<1e-7:
                continue
              
              mask_lbl[lbl_u,lbl_v]=1
              
              int_idx=np.argmax(n_logits_value[0,:,:,channel])
              int_u=int_idx//64
              int_v=int_idx%64
              mask_int[int_u,int_v]=1

              pred_idx=np.argmax(n_logits_value[0,:,:,channel])
              pred_u=pred_idx//64
              pred_v=pred_idx%64
              mask_pred[pred_u,pred_v]=1
              for channel_idx in [channel,int((channel+51)%102)]: #both heatmaps and other non-maps
                tmp_prefix=tmp_path + tmp_header+"_2s"+str(channel_idx)
                self.plt_save(lbls_value[:,:,channel_idx], tmp_prefix+"_lbl.png",True)
                self.plt_save(n_logits_value[0,:,:,channel_idx], tmp_prefix+"_int.png",True)
                self.plt_save(n_logits_value[0,:,:,102+channel_idx], tmp_prefix+"_pred.png",True)

          tmp_prefix=tmp_path+tmp_header
          self.plt_save(mask_lbl,tmp_prefix+"_2mask_lbl.png")
          self.plt_save(mask_int,tmp_prefix+"_2mask_int.png")
          self.plt_save(mask_pred,tmp_prefix+"_2mask_pred.png")

          giant_res=np.concatenate((huge_lbl,huge_int,huge_pred),axis=1)
          self.plt_save(huge_lbl,tmp_prefix+"_2hm_lbl.png",True)
          self.plt_save(huge_int,tmp_prefix+"_2hm_int.png",True)
          self.plt_save(huge_pred,tmp_prefix+"_2hm_pred.png",True)
          self.plt_save(giant_res,tmp_prefix+"_2hm_total.png",True)


          ## 3. write log(loss1,int_pred_value, pred_value)
          with open(tmp_path+tmp_header+"_3log"+str(n_train_loss_value)+".txt","w") as f:
            f.write("helloworld")

          # 4. write npy file
          np.save(tmp_path+tmp_header+"_4lbls.npy",lbls_value)
          np.save(tmp_path+tmp_header+"_4logits.npy",n_logits_value)

        
        duration_total = time.time() - start_time
        
        # gather loss
        self.train_loss_list.append(train_loss_val)
        self.global_train_loss_list.append(train_loss_val)
        self.epoch_train_loss_list.append(train_loss_val)

        # calculate and gather accuracy
        # we keep two methods accuracy
        # for both train,valid
        # 1. we use PCK metrics, then for each channel,
        #    we watch {cands} and {valids}
        #    store with scalar in step, and list in total
        # 2. we use dist metrics, then for each channel,
        #    we watch a scalar
        #    store with scalr in step, and list in total 

        # we calculate acc1/2:
        # 1. among this epoch utill now
        # 2. both for each joints, and in mean
        # 3. (on train set, valid set)
        

        # acc
        train_acc_cands,train_acc_valids,train_acc_val,train_acc2=\
          self._cal_acc_metrics(train_pred_stack, feed_dict)

        self.epoch_train_acc_cands+=train_acc_cands
        self.epoch_train_acc_valids+=train_acc_valids
        self.train_acc_list.append(train_acc_val)
        self.epoch_train_accs.append(train_acc_val)
        
        # Print status to stdout.
        print('Epoch %d Step %d loss=%.5f acc=%.5f (train: %.3f sec, total: %.3f sec)'
              % (self.step / steps_per_epoch, self.step, train_loss_val, train_acc_val,
                  duration_train_step, duration_total))
        
        # Write the summaries
        if self.step % self.TRAIN["summary_freq"] == 0:
          # write loss summary
          #train_loss_op = self.train_loss.assign(train_loss_val)
          #self.sess.run(train_loss_op)#,options=options, run_metadata=run_metadata)
          
          #self._accuracy_computation(logits=self.logits_train_list, gtMaps=self.lbls_pl_list)
          #joints_accur1,joints_accur2=self.sess.run([self.joints_accur1,self.joints_accur2], feed_dict=feed_dict)#,options=options, run_metadata=run_metadata)

          #class_acc=np.array(joints_accur2, dtype = np.float32)
          #mean_acc=np.mean(class_acc)
          
          #TODO feed value to mean_acc
          #mean_acc=train_acc_val
          #self.sess.run(self.train_batch_acc.assign(mean_acc))#,options=options, run_metadata=run_metadata)

          # Update the events file only if I wont do that later on
          print("Saving summaries")
          self.summary_str = self.sess.run(self.summary, feed_dict=feed_dict)#,options=options, run_metadata=run_metadata)
          # add_summary takes ints, so x axis in log will be epoch * 1000
          fake_epoch = int(self.step / float(steps_per_epoch) * 1000)
          self.summary_writer.add_summary(self.summary_str, fake_epoch)
          self.summary_writer.flush()


        ## When comes to an end for an epoch
        # Save a checkpoint and evaluate the model periodically.
        #FIXME this is for testing, change the report location step        
        if (self.step+1) % acc_report_steps == 0 or (self.step + 1) == max_steps:
          
          # DO NOT Evaluate against the training set!
          # Instead, use stored result among each training step
          print('Training Data Eval:')

          # calcualte the epoch average loss and acc
          epoch_loss_mean=np.mean(self.epoch_train_loss_list)
          print("Average Loss: %.5f"%(epoch_loss_mean))
          epoch_acc_mean,epoch_acc2_mean=self.report_accuracy(
            self.epoch_train_acc_cands,self.epoch_train_acc_valids, self.epoch_train_accs)
          
          self.global_train_acc_list.append(epoch_acc_mean)
          self.global_train_acc2_list.append(epoch_acc2_mean)

          #ignore_last = self.TRAIN["ignore_crap"]
          #m_acc = self.training_dataset_accuracy(
          #    self.dataset.train, self.batch_size, self.batch_size_gpu, ignore_last)
          #acc_op = self.train_accuracy.assign(epoch_acc_mean)
          #self.sess.run(acc_op)#,options=options, run_metadata=run_metadata)  # assign the value to the nodes

          # Evaluate against the validation set.
          print('Validation Data Eval:')
          m_acc = self.valid_dataset_accuracy(
              self.dataset.validation, self.batch_size, self.batch_size_gpu)

          #m_acc = self.training_dataset_accuracy(
          #    self.dataset.validation, self.batch_size, self.batch_size_gpu, ignore_last)
          #acc_op = self.validation_accuracy.assign(m_acc)
          #iou_op = self.validation_IoU.assign(m_iou)
          #self.sess.run(acc_op)#,options=options, run_metadata=run_metadata)  # assign the value to the nodes



          # if the validation performance is the best yet, replace saved model
          if m_acc > self.best_acc_validation:
            self.bestValidAtEpoch= int(self.step / float(steps_per_epoch))
            acc_log_folder = self.log + "/acc/"
            if not tf.gfile.Exists(acc_log_folder):
              tf.gfile.MakeDirs(acc_log_folder)
            # save a checkpoint
            self.best_acc_validation = m_acc
            self.acc_checkpoint_file = os.path.join(
                acc_log_folder, 'model-best-acc.ckpt')
            self.saver.save(self.sess, self.acc_checkpoint_file)
            # report to user
            print("Best validation accuracy yet, saving network checkpoint")

          # # Evaluate against the test set.
          # print('Test Data Eval:')
          # m_acc = self.training_dataset_accuracy(
          #     self.dataset.test, self.batch_size, self.batch_size_gpu, ignore_last)
          # acc_op = self.test_accuracy.assign(m_acc)
          # # iou_op = self.test_IoU.assign(m_iou)
          # self.sess.run(acc_op)#,options=options, run_metadata=run_metadata)  # assign the value to the nodes
          
          print("Valid best at", self.bestValidAtEpoch,"with", self.best_acc_validation)
          # summarize
          self.summary_str = self.sess.run(self.summary, feed_dict=feed_dict)#,options=options, run_metadata=run_metadata)
          # add_summary takes ints, so x axis in log will be epoch * 1000
          fake_epoch = int(self.step / float(steps_per_epoch) * 1000)
          self.summary_writer.add_summary(self.summary_str, fake_epoch)
          self.summary_writer.flush()

  def placeholders(self, depth, batch_size):
    """Generate placeholder variables to represent the input tensors
    Args:
      depth ~[depth_0, depth_1] for [img, heatmap]
      batch_size: The batch size will be baked into both placeholders.
    Return:
      img_pl: placeholder for inputs
      lbls_pl: placeholder for labels
    """

    img_pl = tf.placeholder(tf.float32, shape=(
        batch_size, 256, 256, depth[0]), name="x_pl")
    lbls_pl = tf.placeholder(tf.float32, shape=(
        batch_size, 64, 64, depth[1]), name="y_pl")
    return img_pl, lbls_pl

  def report_accuracy(self, epoch_acc_cands, epoch_acc_valids, epoch_acc_list):
    epoch_acc_joints=np.zeros((1,self.num_classes))
    for j in range(self.num_classes):
     if self.almost_zero(epoch_acc_valids[0,j]):
       epoch_acc_joints[0,j]=-1
     else:
       epoch_acc_joints[0,j]=epoch_acc_cands[0,j]/epoch_acc_valids[0,j]
    if self.almost_zero(np.sum(epoch_acc_valids)):
      epoch_acc_mean=-1
    else:
      epoch_acc_mean=np.sum(epoch_acc_cands)/np.sum(epoch_acc_valids)
    
    epoch_acc2_mean=np.mean(epoch_acc_list)

    print('   Mean Accuracy_1: %0.04f' % (epoch_acc_mean))
    print('   Mean Accuracy_2: %0.04f' % (epoch_acc2_mean))
    for class_i,sub_list in enumerate(index_list):
        class_acc=[]
        class_string=""
        for idx in sub_list:
          class_acc.append(epoch_acc_joints[0,idx])
          class_string+='%.4f '% epoch_acc_joints[0,idx]
        class_acc=np.array(class_acc)
        if sum(class_acc>1e-7)==0:
          class_acc_mean=-1
        else:
          class_acc_mean=np.mean(class_acc[class_acc>1e-7])
        print('     class %s Precision: %.4f' % (class_list[class_i], class_acc_mean ))
        print('     '+class_string)
    
    return epoch_acc_mean,epoch_acc2_mean

  def valid_dataset_accuracy(self, dataset, batch_size, batch_size_gpu):
    ''' Faster tensorflow metrics using tensorflow confusion matrix,
        for training
    '''
    # define accuracy metric for this model
    start_time_overall = time.time()  # save curr time to report duration
    inference_time = 0.0
    steps_per_epoch = self.TRAIN["steps_per_epoch"] #dataset.num_examples // batch_size
    steps_per_dataset=dataset.num_examples // batch_size
    assert(steps_per_dataset > 0 and "Dataset length should be more than batchsize")
    num_examples = steps_per_dataset * batch_size
    for step in range(steps_per_dataset):
      if step==0 and dataset.name=="test":
        if self.first_time_do_this:
          print("I'm first time do this")
          self.first_time_do_this=False
          self.the_feed_dict,self.names=self.fill_feed_dict(
            dataset, self.img_pl_list, self.lbls_pl_list, batch_size_gpu)
        print("Now old friends",self.names)
        feed_dict= self.the_feed_dict
        names=self.names
      else:
        feed_dict, names = self.fill_feed_dict(
            dataset, self.img_pl_list, self.lbls_pl_list, batch_size_gpu)
      inference_start = time.time()
      

      valid_loss_val, valid_pred_stack=       \
          self.sess.run(
            [self.n_valid_loss, self.n_logits_valid_stack ],\
            feed_dict=feed_dict
          )
      
      inference_time += time.time() - inference_start
      
      # add loss
      self.valid_loss_list.append(valid_loss_val)
      self.global_valid_loss_list.append(valid_loss_val)
      self.epoch_valid_loss_list.append(valid_loss_val)
      

      # add acc
      valid_acc_cands,valid_acc_valids,valid_acc_val,valid_acc2=self._cal_acc_metrics(
          valid_pred_stack, feed_dict)

      self.epoch_valid_acc_cands+=valid_acc_cands
      self.epoch_valid_acc_valids+=valid_acc_valids
      self.valid_acc_list.append(valid_acc_val)
      self.epoch_valid_accs.append(valid_acc_val)

      #TODO add acc2

      #TODO## insert test visual code section here ...)
      

    # finally, after training on whole valid dataset  
    epoch_loss_mean=np.mean(self.epoch_valid_loss_list)
    print("Average Loss: %.5f"%(epoch_loss_mean))


    overall_duration = time.time() - start_time_overall  # calculate time elapsed
    print('   Num samples: %d, Time to run %.3f sec (only inference: %.3f sec)' %
          (num_examples, overall_duration, inference_time))
    fps = (num_examples / inference_time)
    print('   Network FPS: %.3f' % fps)
    print('   Time per image: %.3f s' % (1 / fps))
    

    # report the acc result
    epoch_acc_mean,epoch_acc2_mean=self.report_accuracy(
      self.epoch_valid_acc_cands,self.epoch_valid_acc_valids, self.epoch_valid_accs)
    
    self.global_valid_acc_list.append(epoch_acc_mean)
    self.global_valid_acc2_list.append(epoch_acc2_mean)

    train_leg, =plt.plot(self.global_train_acc_list, label="train", color="red")
    valid_leg, =plt.plot(self.global_valid_acc_list, label="valid", color="blue")
    #test_leg, =plt.plot(self.test_acc_list, label="test", color="green")
    plt.xlabel("every "+str(self.TRAIN["acc_report_epochs"])+" epochs")
    plt.ylabel("accuracy")
    plt.legend(handles=[train_leg,valid_leg])#,test_leg])
    path_to_save = self.log + '/predictions/'
    if not tf.gfile.Exists(path_to_save):
      tf.gfile.MakeDirs(path_to_save)
    plt.savefig(path_to_save+"acc_log.png")
    plt.close()
    
    train_leg, =plt.plot(self.global_train_acc2_list, label="train", color="red")
    valid_leg, =plt.plot(self.global_valid_acc2_list, label="valid", color="blue")
    #test_leg, =plt.plot(self.test_acc_list, label="test", color="green")
    plt.xlabel("every "+str(self.TRAIN["acc_report_epochs"])+" epochs")
    plt.ylabel("accuracy2")
    plt.legend(handles=[train_leg,valid_leg])#,test_leg])
    path_to_save = self.log + '/predictions/'
    if not tf.gfile.Exists(path_to_save):
      tf.gfile.MakeDirs(path_to_save)
    plt.savefig(path_to_save+"acc2_log.png")
    plt.close()

    

    if len(self.train_loss_list)>2:
      plt.plot(self.train_loss_list)
      plt.xlabel("steps")
      plt.ylabel("loss")
      plt.savefig(path_to_save+"loss_log.png")
      plt.close()
      for epi_i in range(1,(int)(1000/10)):
        if len(self.train_loss_list)>epi_i*10*steps_per_epoch:
          plt.plot([i+(epi_i-1)*10*steps_per_epoch for i in range(len(self.train_loss_list[(epi_i-1)*10*steps_per_epoch:epi_i*10*steps_per_epoch]))],self.train_loss_list[(epi_i-1)*10*steps_per_epoch:epi_i*10*steps_per_epoch])
          plt.xlabel("steps")
          plt.ylabel("loss")
          plt.savefig(path_to_save+"loss_log_"+str(epi_i-1)+".png")
          plt.close()

    #TODO DISPLAY AND DEBUGGING CODE(see appendix B)

    return epoch_acc_mean

  def training_dataset_accuracy(self, dataset, batch_size, batch_size_gpu,
                                ignore_last=False):
    ''' Faster tensorflow metrics using tensorflow confusion matrix,
        for training
    '''
    # define accuracy metric for this model
    start_time_overall = time.time()  # save curr time to report duration
    inference_time = 0.0
    steps_per_epoch = self.TRAIN["steps_per_epoch"] #dataset.num_examples // batch_size
    steps_per_dataset=dataset.num_examples // batch_size
    assert(steps_per_dataset > 0 and "Dataset length should be more than batchsize")
    num_examples = steps_per_dataset * batch_size
    for step in range(steps_per_dataset):
      if step==0 and dataset.name=="test":
        if self.first_time_do_this:
          print("I'm first time do this")
          self.first_time_do_this=False
          self.the_feed_dict,self.names=self.fill_feed_dict(
            dataset, self.img_pl_list, self.lbls_pl_list, batch_size_gpu)
        print("Now old friends",self.names)
        feed_dict= self.the_feed_dict
        names=self.names
      else:
        feed_dict, names = self.fill_feed_dict(
            dataset, self.img_pl_list, self.lbls_pl_list, batch_size_gpu)
      inference_start = time.time()
      joints_accur1, joints_accur2, pred=self.sess.run([self.joints_accur1,self.joints_accur2, self.logits_valid],feed_dict=feed_dict)
      inference_time += time.time() - inference_start

      if step==0 and dataset.name=="test":
        for g in range(0,self.n_gpus):
          for idx in range(0,batch_size_gpu):
            name=names[g][idx]
            img=feed_dict[self.img_pl_list[g]][idx]
            label=feed_dict[self.lbls_pl_list[g]][idx]
            proper_w=self.DATA["img_prop"]["width"]
            proper_h=self.DATA["img_prop"]["height"]
            h,w,c=label.shape

            htmp0_img=np.zeros((256,256))
            if self.DATA["multi_class"]==False:
              monitor_index = 9
            else:
              monitor_index = 0

            #TODO hard code here
            label_img=np.zeros((256,256))
            kpmps_img=np.zeros((256,256))
            raw_err_img=np.zeros((64,64),dtype=np.uint8)
            err_img=np.zeros((256,256),dtype=np.uint8) 
            for i in range(c):
              arg_loc=np.argmax(label[:,:,i])
              if label[int(arg_loc/w),arg_loc%w,i]<1e-7:
                continue
              if self.DATA["multi_class"]:
                monitor_index = i

            for i in range(c):
              arg_loc=np.argmax(label[:,:,i])
              loc2=arg_loc
              if label[int(arg_loc/w),arg_loc%w,i]<1e-7:
                continue # this is not a valid layer, so don't draw on vis heatmaps
              label_img[int(arg_loc/w)*4:int(arg_loc/w)*4+4,arg_loc%w*4:arg_loc%w*4+4]=255
              if i == monitor_index:
                label_img[int(arg_loc/w)*4:int(arg_loc/w)*4+8,arg_loc%w*4:arg_loc%w*4+8]=255
              #if self.DATA["multi_class"]:
              #  monitor_index = i

              arg_loc=np.argmax(pred[idx,:,:,i])
              loc1=arg_loc
              kpmps_img[int(arg_loc/w)*4:int(arg_loc/w)*4+4,arg_loc%w*4:arg_loc%w*4+4]=255
              
              rr, cc = line((int)(loc1/w), loc1%w, (int)(loc2/w), loc2%w)
              raw_err_img[rr, cc] = 255
            
            for pred_i in range(h):
              for pred_j in range(w):
                htmp0_img[pred_i*4:pred_i*4+4,pred_j*4:pred_j*4+4]=pred[idx,pred_i,pred_j,monitor_index] # find head!
                
            htmp0_img=htmp0_img-np.min(htmp0_img) 
            v_range=np.max(htmp0_img)-np.min(htmp0_img)+0.0001
            htmp0_img=(htmp0_img/v_range*255).astype(int)

            for i in range(h):
              for j in range(w):
                err_img[i*4:i*4+4,j*4:j*4+4]=raw_err_img[i,j]
            font = cv2.FONT_HERSHEY_SIMPLEX
            img_clone=np.array(img)
            cv2.putText(img_clone,str(monitor_index),(50,200), font, 4,(0,0,0),2,cv2.LINE_AA)
            img_grey=np.mean(img_clone,axis=2).astype(int)

            path_to_save = self.log + '/predictions/'

            if not tf.gfile.Exists(path_to_save):
              tf.gfile.MakeDirs(path_to_save)
            
            if name not in self.counter_dict:
              self.counter_dict[name]=0

            cv2.imwrite(path_to_save + dataset.name + '_' + str(name)+'_'+str(self.counter_dict[name]),
                         np.concatenate((img_grey, label_img, htmp0_img, kpmps_img,err_img), axis=1))
            self.counter_dict[name]+=1
    
    #mean_acc= np.mean(np.array(joints_accur, dtype = np.float32))
    valid_joints_accur1=np.array(joints_accur1, dtype = np.float32)
    valid_joints_accur1=valid_joints_accur1[valid_joints_accur1>1e-7]

    valid_joints_accur2=np.array(joints_accur2, dtype = np.float32)
    valid_joints_accur2=valid_joints_accur2[valid_joints_accur2>1e-7]


    mean_acc1= np.mean(valid_joints_accur1)
    mean_acc2= np.mean(valid_joints_accur2)

    overall_duration = time.time() - start_time_overall  # calculate time elapsed
    print('   Num samples: %d, Time to run %.3f sec (only inference: %.3f sec)' %
          (num_examples, overall_duration, inference_time))
    fps = (num_examples / inference_time)
    print('   Network FPS: %.3f' % fps)
    print('   Time per image: %.3f s' % (1 / fps))
    print(" Pixelwise Performance: ")
    print('   Mean Accuracy_1: %0.04f' % (mean_acc1))
    print('   Mean Accuracy_2: %0.04f' % (mean_acc2))
    if dataset.name=="train":
      self.train_acc_list.append(mean_acc2)
    if dataset.name=="valid":
      self.valid_acc_list.append(mean_acc2)
    if dataset.name=="test":
      self.test_acc_list.append(mean_acc2)
      
      train_leg, =plt.plot(self.train_acc_list, label="train", color="red")
      valid_leg, =plt.plot(self.valid_acc_list, label="valid", color="blue")
      test_leg, =plt.plot(self.test_acc_list, label="test", color="green")
      plt.xlabel("every "+str(self.TRAIN["acc_report_epochs"])+" epochs")
      plt.ylabel("accuracy")
      plt.legend(handles=[train_leg,valid_leg,test_leg])
      path_to_save = self.log + '/predictions/'
      if not tf.gfile.Exists(path_to_save):
        tf.gfile.MakeDirs(path_to_save)
      plt.savefig(path_to_save+"acc_log.png")
      plt.close()
      
      if len(self.train_loss_list)>2:
        plt.plot(self.train_loss_list)
        plt.xlabel("steps")
        plt.ylabel("loss")
        plt.savefig(path_to_save+"loss_log.png")
        plt.close()
        for epi_i in range(1,(int)(1000/10)):
          if len(self.train_loss_list)>epi_i*10*steps_per_epoch:
            plt.plot([i+(epi_i-1)*10*steps_per_epoch for i in range(len(self.train_loss_list[(epi_i-1)*10*steps_per_epoch:epi_i*10*steps_per_epoch]))],self.train_loss_list[(epi_i-1)*10*steps_per_epoch:epi_i*10*steps_per_epoch])
            plt.xlabel("steps")
            plt.ylabel("loss")
            plt.savefig(path_to_save+"loss_log_"+str(epi_i-1)+".png")
            plt.close()
    
    if self.DATA["multi_class"]==False:
      print('   Precision:')
      for idx in range(len(joints_accur2)):
        print('     class %d Precision: %f' % (idx, joints_accur2[idx]))
    else:

      print('   Multi-precision:')
      for class_i,sub_list in enumerate(index_list):
        
        class_acc=[]
        class_string=""
        for idx in sub_list:
          class_acc.append(joints_accur1[idx])
          class_string+='%.4f '% joints_accur1[idx]
        class_acc=np.array(class_acc)
        print('     class %s Precision: %.4f' % (class_list[class_i], np.mean(class_acc[class_acc>1e-7]) ))
        print('     '+class_string)

      print('   Precision:')
      for idx in range(len(joints_accur2)):
        print('     class %d Precision: %f' % (idx, joints_accur2[idx]))

    return mean_acc2 #, joints_accur  #mean_acc, mean_iou, per_class_iou, per_class_prec, per_class_rec

  def _argmax(self, tensor):
    """ ArgMax
    Args:
      tensor	: 2D - Tensor (Height x Width : 64x64 )
    Returns:
      arg		: Tuple of max position
    """
    resh = tf.reshape(tensor, [-1])
    argmax = tf.arg_max(resh, 0)
    return (argmax // tensor.get_shape().as_list()[0], argmax % tensor.get_shape().as_list()[0])
  
  def predict_kickstart(self, path, batchsize=1):
    # bake placeholders
    self.img_pl, self.lbls_pl = self.placeholders(
        [self.DATA["img_prop"]["depth"], self.DATA["img_prop"]["hm_depth"]], batchsize)

    # make list
    self.n_gpus = 1
    self.img_pl_list = [self.img_pl]
    self.lbls_pl_list = [self.lbls_pl]

    # inititialize inference graph
    print("Initializing network")
    with tf.name_scope("test_model"):
      with tf.variable_scope("model", reuse=None):
        self.logits_valid, self.code_valid, self.n_img_valid = self.build_graph(
            self.img_pl, False)  # not training

    # lists of outputs
    self.logits_valid_list = [self.logits_valid]
    self.logits_code_list = [self.code_valid]

    # get model size and report it (so that I can report in paper)
    n_parameters = 0
    #for var in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,scope='model'):
    for var in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='model'):
      # print(var.name , var.get_shape().as_list(), np.prod(var.get_shape().as_list()))
      #print(var.name)
      var_params = np.prod(var.get_shape().as_list())
      n_parameters += var_params


    print("*" * 80)
    print("Total number of parameters in network: ",
          "{:,}".format(n_parameters))
    print("*" * 80)

    # build graph and predict value (if graph is not built)
    print("Predicting mask")

    # set up evaluation head in the graph
    with tf.variable_scope("output"):
      self.output_p = tf.nn.softmax(self.logits_valid)
      self.mask = tf.argmax(self.output_p, axis=3, output_type=tf.int32)
      #TODO other probes we need(in the future)

    # report the mask shape as a sort of sanity check
    mask_shape = self.mask.get_shape().as_list()
    print("mask shape", mask_shape)

    # metadata collector for verbose mode (spits out layer-wise profile)
    self.run_metadata = tf.RunMetadata()

    # Add the variable initializer Op.
    self.init = tf.global_variables_initializer()

    # Create a saver for restoring and saving checkpoints.
    self.saver = tf.train.Saver(save_relative_paths=True)

    # xla stuff for faster inference (and soft placement for low ram device)
    gpu_options = tf.GPUOptions(allow_growth=True, force_gpu_compatible=True)
    config = tf.ConfigProto(allow_soft_placement=True,
                            log_device_placement=False, gpu_options=gpu_options)
    config.graph_options.optimizer_options.global_jit_level = tf.OptimizerOptions.ON_2

    # start a session
    self.sess = tf.Session(config=config)

    # init variables
    self.sess.run(self.init)

    # if path to model is give, try to restore:
    self.restore_session(path)

    print("Saving this graph in %s" % self.log)
    self.summary_writer = tf.summary.FileWriter(self.log, self.sess.graph)
    self.summary_writer.flush()

    # save this graph
    self.chkpt_graph = os.path.join(self.log, 'model.ckpt')
    self.saver.save(self.sess, self.chkpt_graph)
    tf.train.write_graph(self.sess.graph_def, self.log, 'model.pbtxt')

  def freeze_graph(self, path=None, verbose=False):
    """ Extract the sub graph defined by the output nodes and convert
        all its variables into constant
    """
    # kickstart the model. If session is initialized everything may be dirty,
    # so please use this function from a clean tf environment :)
    if self.sess is None:
      self.predict_kickstart(path)
    else:
      print("existing session. This is unintended behavior. Check!")
      quit()

    # outputs
    in_node_names = [str(self.img_pl.op.name)]
    print("in_node_names", in_node_names)
    in_trt_node_names = [str(self.n_img_valid.op.name)]
    print("in_tensorRT_node_names", in_trt_node_names)
    out_node_names = [str(self.mask.op.name), str(self.code_valid.op.name)]
    print("out_node_names", out_node_names)
    input_graph_path = os.path.join(self.log, 'model.pbtxt')
    checkpoint_path = os.path.join(self.log, 'model.ckpt')
    input_saver_def_path = ""
    input_binary = False
    restore_op_name = "save/restore_all"
    filename_tensor_name = "save/Const:0"
    out_frozen_graph_name_nchw = os.path.join(self.log, 'frozen_nchw.pb')
    out_frozen_graph_name_nhwc = os.path.join(self.log, 'frozen_nhwc.pb')
    out_opt_graph_name = os.path.join(self.log, 'optimized.pb')
    out_opt_tensorRT_graph_name = os.path.join(self.log, 'optimized_tRT.pb')
    uff_opt_tensorRT_graph_name = os.path.join(self.log, 'optimized_tRT.uff')
    output_quantized_graph_name = os.path.join(self.log, 'quantized.pb')
    clear_devices = True

    # freeze
    freeze_graph.freeze_graph(input_graph_path, input_saver_def_path,
                              input_binary, checkpoint_path, ",".join(
                                  out_node_names),
                              restore_op_name, filename_tensor_name,
                              out_frozen_graph_name_nhwc, clear_devices, "")

    # Optimize for inference
    input_graph_def = tf.GraphDef()
    with tf.gfile.Open(out_frozen_graph_name_nhwc, "rb") as f:
      data = f.read()
      input_graph_def.ParseFromString(data)

    # transforms for optimization
    transforms = ['add_default_attributes',
                  'remove_nodes(op=Identity, op=CheckNumerics)',
                  'fold_constants(ignore_errors=true)', 'fold_batch_norms',
                  'fold_old_batch_norms',
                  'strip_unused_nodes', 'sort_by_execution_order']

    # optimize and save
    output_graph_def = TransformGraph(input_graph_def,
                                      in_node_names,
                                      out_node_names,
                                      transforms)
    f = tf.gfile.FastGFile(out_opt_graph_name, "w")
    f.write(output_graph_def.SerializeToString())

    # quantize and optimize, and save
    transforms += ['quantize_weights', 'quantize_nodes']
    output_graph_def = TransformGraph(input_graph_def,
                                      in_node_names,
                                      out_node_names,
                                      transforms)
    f = tf.gfile.FastGFile(output_quantized_graph_name, "w")
    f.write(output_graph_def.SerializeToString())

    # save the names of the input and output nodes
    input_node = str(self.img_pl.op.name)
    input_norm_and_resized_node = str(self.n_img_valid.op.name)
    code_node = str(self.code_valid.op.name)
    logits_node = str(self.logits_valid.op.name)
    out_probs_node = str(self.output_p.op.name)
    mask_node = str(self.mask.op.name)
    node_dict = {"input_node": input_node,
                 "input_norm_and_resized_node": input_norm_and_resized_node,
                 "code_node": code_node,
                 "logits_node": logits_node,
                 "out_probs_node": out_probs_node,
                 "mask_node": mask_node}
    node_file = os.path.join(self.log, "nodes.yaml")
    with open(node_file, 'w') as f:
      yaml.dump(node_dict, f, default_flow_style=False)

    # do the same for NCHW but don't save any quantized models,
    # since quantization doesn't work in NCHW (only save optimized for tensort)
    self.sess.close()
    tf.reset_default_graph()
  
  
  def normalize(self,array):
    minv=np.min(array)
    maxv=np.max(array)
    if minv==maxv:
      return array
    else:
      return (array-minv)/(maxv-minv)

  def almost_zero(self,x):
    return abs(x)<1e-7

  def plt_save(self,img,path,use_colormap=False):
    #print("save image to",path)
    plt.imshow(img)
    if use_colormap:
      plt.colorbar()
    plt.savefig(path)
    plt.close()

'''
     # APPENDIX A
   
     # WHEN STACKS NOT 2 (previously in function `build_graph_full`)

# Let's forget when stacks are more than 2
      # for i in range(1, self.nStack -1):
      #   with tf.variable_scope('stage_'+str(i)): #tf.name_scope('stage_' + str(i)):
      #     hg[i] = self._hourglass(train_stage, sum_[i-1], self.nLow, self.nFeat*2, 'hourglass',data_format=data_format)
      #     if self.TRAIN["use_dropout"]:
      #       drop[i] = tf.layers.dropout(hg[i], rate = self.dropout_rate, training = train_stage, name = 'dropout')
      #       #FIXME follow by pavlakos code
      #       #ll[i] = self._conv_bn_relu(train_stage, drop[i], self.nFeat, 1, 1, 'VALID', name= 'conv', data_format=data_format)
      #       ll[i] = self._conv_bn_relu(train_stage, drop[i], self.nFeat*2, 1, 1, 'VALID', name= 'conv', data_format=data_format)
      #     else:
      #       #FIXME follow by pavlakos code
      #       #ll[i] = self._conv_bn_relu(train_stage, hg[i], self.nFeat, 1, 1, 'VALID', name= 'conv', data_format=data_format)
      #       ll[i] = self._conv_bn_relu(train_stage, hg[i], self.nFeat*2, 1, 1, 'VALID', name= 'conv', data_format=data_format)
      #     #FIXME follow by pavlakos code
      #     #ll_[i] = self._conv(train_stage, ll[i], self.nFeat, 1, 1, 'VALID', 'll', data_format=data_format)
      #     ll_[i] = self._conv_bn_relu(train_stage, ll[i], self.nFeat, 1, 1, 'VALID', 'll', data_format=data_format)
      #     if self.modif:
      #       out[i] = self._conv_bn_relu(train_stage, ll[i], self.outDim, 1, 1, 'VALID', 'out', data_format=data_format)
      #     else:
      #       out[i] = self._conv(train_stage, ll[i], self.outDim, 1, 1, 'VALID', 'out', data_format=data_format)
      #     out_[i] = self._conv(train_stage, out[i], self.nFeat, 1, 1, 'VALID', 'out_', data_format=data_format)
      #     sum_[i] = tf.add_n([out_[i], sum_[i-1], ll_[0]], name= 'merge')
'''

'''
    #APPENDIX B

    #DISPLAY AND DEBUGGING CODE (previously in function `valid_dataset_accuracy`)

    # print(" Pixelwise Performance: ")
    # print('   Mean Accuracy_1: %0.04f' % (mean_acc1))
    # print('   Mean Accuracy_2: %0.04f' % (mean_acc2))
    # if dataset.name=="train":
    #   self.train_acc_list.append(mean_acc2)
    # if dataset.name=="valid":
    #   self.valid_acc_list.append(mean_acc2)
    # if dataset.name=="test":
    #   self.test_acc_list.append(mean_acc2)
      
    #   train_leg, =plt.plot(self.train_acc_list, label="train", color="red")
    #   valid_leg, =plt.plot(self.valid_acc_list, label="valid", color="blue")
    #   test_leg, =plt.plot(self.test_acc_list, label="test", color="green")
    #   plt.xlabel("every "+str(self.TRAIN["acc_report_epochs"])+" epochs")
    #   plt.ylabel("accuracy")
    #   plt.legend(handles=[train_leg,valid_leg,test_leg])
    #   path_to_save = self.log + '/predictions/'
    #   if not tf.gfile.Exists(path_to_save):
    #     tf.gfile.MakeDirs(path_to_save)
    #   plt.savefig(path_to_save+"acc_log.png")
    #   plt.close()
      
    #   if len(self.train_loss_list)>2:
    #     plt.plot(self.train_loss_list)
    #     plt.xlabel("steps")
    #     plt.ylabel("loss")
    #     plt.savefig(path_to_save+"loss_log.png")
    #     plt.close()
    #     for epi_i in range(1,(int)(1000/10)):
    #       if len(self.train_loss_list)>epi_i*10*steps_per_epoch:
    #         plt.plot([i+(epi_i-1)*10*steps_per_epoch for i in range(len(self.train_loss_list[(epi_i-1)*10*steps_per_epoch:epi_i*10*steps_per_epoch]))],self.train_loss_list[(epi_i-1)*10*steps_per_epoch:epi_i*10*steps_per_epoch])
    #         plt.xlabel("steps")
    #         plt.ylabel("loss")
    #         plt.savefig(path_to_save+"loss_log_"+str(epi_i-1)+".png")
    #         plt.close()
    
    # if self.DATA["multi_class"]==False:
    #   print('   Precision:')
    #   for idx in range(len(joints_accur2)):
    #     print('     class %d Precision: %f' % (idx, joints_accur2[idx]))
    # else:

    #   print('   Multi-precision:')
    #   for class_i,sub_list in enumerate(index_list):
        
    #     class_acc=[]
    #     class_string=""
    #     for idx in sub_list:
    #       class_acc.append(joints_accur1[idx])
    #       class_string+='%.4f '% joints_accur1[idx]
    #     class_acc=np.array(class_acc)
    #     print('     class %s Precision: %.4f' % (class_list[class_i], np.mean(class_acc[class_acc>1e-7]) ))
    #     print('     '+class_string)

    #   print('   Precision:')
    #   for idx in range(len(joints_accur2)):
    #     print('     class %d Precision: %f' % (idx, joints_accur2[idx]))

    #return mean_acc2 #, joints_accur  #mean_acc, mean_iou, per_class_iou, per_class_prec, per_class_rec
'''

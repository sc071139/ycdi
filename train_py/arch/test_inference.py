import tensorflow as tf
import numpy as np
import cv2
import time
import sys
import os
from tensorflow.python.tools import inspect_checkpoint as chkp

# print all tensors in checkpoint file
#chkp.print_tensors_in_checkpoint_file("../cfg/pascal/logs-bak-0717-aug-on-fly/acc/model-best-acc.ckpt", tensor_name='', all_tensors=True)

use_freeze_graph=True

if use_freeze_graph:
    frozen_name="../cfg/pascal/f-logs/frozen_nhwc.pb"
    with tf.Graph().as_default() as graph:
        try:
            with tf.gfile.GFile(frozen_name, "rb") as f:
                graph_def = tf.GraphDef()
                graph_def.ParseFromString(f.read())
        except:
            print("Failed to extract grapfdef. Exiting...")
            quit()
        
        input_node_str="x_pl:0"
        output_node_str="test_model/model/stage_1/out/conv/BiasAdd:0"
        input_node,output_node=tf.import_graph_def(graph_def, return_elements=[input_node_str,output_node_str])

        gpu_options = tf.GPUOptions(allow_growth=True, force_gpu_compatible=True)
        config = tf.ConfigProto(allow_soft_placement=True,
                            log_device_placement=False, gpu_options=gpu_options)
        config.graph_options.optimizer_options.global_jit_level = tf.OptimizerOptions.ON_2
        sess= tf.Session(config=config)
        for i in range(200):
            t0=time.time()
            img=cv2.imread("test/input_%d.jpg"%(i%3)) 
            t1=time.time()
            heatmaps=sess.run(output_node, feed_dict={input_node:[img/255]})
            t2=time.time()
            out_img=np.zeros((64,64))
            for j in range(np.shape(heatmaps)[3]):
                idx=np.argmax(heatmaps[0,:,:,j])
                out_img[idx//64,idx%64]=255
            cv2.imwrite("test/output_"+str(i)+".jpg",out_img)
            t3=time.time()
            print("load: %.6f infer: %.6f save: %.6f"%(t1-t0,t2-t1,t3-t2))

else:
    net_path="../cfg/pascal/logs-bak-0717-aug-on-fly/acc/model-best-acc.ckpt.meta"
    n_time=20

    sess= tf.Session()

    saver = tf.train.import_meta_graph(net_path)
    saver.restore(sess, tf.train.latest_checkpoint('../cfg/pascal/logs-bak-0717-aug-on-fly/acc/'))

    graph=tf.get_default_graph()
    print(graph)   
#    for var in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLE):
        # print(var.name , var.get_shape().as_list(), np.prod(var.get_shape().as_list()))
#        print(var.name)
        #                 var_params = np.prod(var.get_shape().as_list())
        #                        n_parameters += var_params




    input_node = graph.get_tensor_by_name("test_model/model/x_pl:0")
    output_node = graph.get_tensor_by_name("test_model/model/stage_1/out/conv/BiasAdd:0")

    for i in range(n_time):
        t0=time.time()
        img=cv2.imread("test/input_%d.jpg"%(i%3)) 
        t1=time.time()
        heatmaps=sess.run(output_node, feed_dict={input_node:[img/255]})
        t2=time.time()
        out_img=np.zeros((64,64))
        for j in range(np.shape(heatmaps)[3]):
            idx=np.argmax(heatmaps[0,:,:,j])
            out_img[idx//64,idx%64]=255
        cv2.imwrite("test/output_"+str(i)+".jpg",out_img)
        t3=time.time()
        print("load: %.6f infer: %.6f save: %.6f"%(t1-t0,t2-t1,t3-t2))

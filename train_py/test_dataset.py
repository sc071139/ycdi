import yaml
import imp
import os

source=open("test_cfg.sh","r")
cfg=source.readlines()[1].strip().split("=")[1]
print("test on", cfg)
f=open("cfg/"+cfg+'/data.yaml','r')
DATA=yaml.load(f)
parser=imp.load_source("parser",os.getcwd()+'/dataset/'+DATA["name"]+'.py')
parser.read_data_sets(DATA)
